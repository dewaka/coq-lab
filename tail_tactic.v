(* Coq tactic to prove equivalance between tail and non-tail recurisve functions *)

(* First some manual proofs *)

Require Import Nat.
Require Import Ring.
Require Import Arith.
Require Import List.

Import ListNotations.

Section FactStuff.

Fixpoint fact (n : nat) :=
  match n with
    | 0 => 1
    | S n' => n * fact n'
  end.

Fixpoint tfact (n m : nat) :=
  match n with
    | 0 => m
    | S n' => tfact n' (m * n)
  end.

Definition fact' (n : nat) : nat := tfact n 1.

(* Just to check *)
Eval compute in fact' 0.
Eval compute in fact 0.
Eval compute in fact' 6.
Eval compute in fact 6.

(*
This is just saying the same thing as,
fact S n = S n * fact n
which is just there by the definition
*)
Lemma fact_n_plus_1 : forall n,
                        fact (n + 1) = fact n * (n + 1).
Proof.
  intro n.
  SearchAbout (_ * _ = _ * _).
  rewrite PeanoNat.Nat.mul_comm.
  assert (Sn : S n = n + 1).    (* There must be a simpler way to get S n = n + 1 *)
    induction n.
    auto.
    rewrite IHn.
    SearchAbout (S (_ + _)).
    rewrite plus_n_Sm.
    ring.
  rewrite <- Sn.
  auto.
Qed.

(*
What general fact do we need to prove about tfact accumulator
How does the accumulator behave...
*)

(*
This is something useful about tfact.
Same as saying,
   fact' S n = tfact n (S n)
*)
Lemma tfact_misc_1 : forall n, tfact (S n) 1 = tfact n (S n).
Proof.
  intro n.
  induction n.
  (* case n = 0 *)
  reflexivity.
  (* case n = S n *)
  unfold tfact.
  simpl.
  (* SearchAbout (_ + 0). *)
  rewrite Nat.add_0_r.
  auto.
Qed.

(* This is just by definition *)
Lemma tfact_misc_2 : forall n m,
                       tfact (S n) m = tfact n (m * (S n)).
Proof. auto. Qed.

(*
How can we use this fact?
Tracing tfact (S n) m
Tracing tfact n) m
tfact n (m * (S n))
tfact (n-1) (m * (S n) * n)
tfact (n-2) (m * (S n) * n * (n-1))
tfact 2 (m * (S n) * n * (n-1) ... 3)
tfact 1 (m * (S n) * n * (n-1) ... 3 * 2)
tfact 0 (m * (S n) * n * (n-1) ... 3 * 2 * 1)

tfact n m = tfact 0 (m * fact n)
          = m * fact n
*)

Lemma tfact_m_fact : forall n m,
                       tfact n m = m * fact n.
Proof.
  intro n.
  induction n.
  intro m.
  simpl.
  ring.
  intro m.
  simpl.
  rewrite (IHn (m * S n)).
  ring.
Qed.
  
(* Tail recursive factorial is equal to simple factorial *)
Lemma tfact_fact'_eq : forall n, fact n = fact' n.
Proof.
  intro n.
  unfold fact'.
  rewrite (tfact_m_fact n 1).
  ring.
Qed.

End FactStuff.

Section ListStuff.

(* Simple reverse *)
Fixpoint reverse {A} (l : list A) : list A :=
  match l with
    | [] => []
    | x :: xs => reverse xs ++ [x]
  end.

Eval compute in reverse [1; 2; 3].

Fixpoint treverse {A} (l : list A) (acc : list A) : list A :=
  match l with
    | [] => acc
    | x :: xs => treverse xs (x :: acc)
  end.

Definition reverse' {A} (l : list A) := treverse l [].

Eval compute in reverse' [1; 2; 3].

Lemma treverse_m_reverse : forall A (l : list A) (m : list A),
                             treverse l m =  reverse l ++ m.
Proof.
  intros l.
  induction l0.
  reflexivity.
  intro m.
  simpl.
  rewrite (IHl0 (a :: m)).
  (* SearchAbout (_ ++ _). *)
  rewrite <- app_assoc.
  reflexivity.
Qed.

(* Tail recursive reverse is equal to simple reverse *)
Lemma treverse_eq_reverse : forall A (l : list A),
                              reverse l = reverse' l.
Proof.
  intros.
  unfold reverse'.
  rewrite (treverse_m_reverse A l []).
  (* SearchAbout (_ ++ []). *)
  rewrite app_nil_r.
  reflexivity.
Qed.        

End ListStuff.

(* This example is from Generalization Lemma paper *)
Section WSumStuff.

Fixpoint wsum (n : nat) : nat :=
  match n with
    | 0 => 0
    | S n' => S n + wsum n'
  end.

Fixpoint wsigma (n y : nat) : nat :=
  match n with
    | 0 => y
    | S n' => wsigma n' (y + (S (S 0)) + n')
  end.

Eval compute in wsum 3.         (* 9 *)
Eval compute in wsigma 3 0.     (* 9 *)
Eval compute in wsigma 3 3.     (* 12 *)

(* This is the intermediate lemma we need with the right generalisation *)
Lemma wsum_m_wsigma : forall n m, wsigma n m = m + wsum n.
Proof.
  intro n.
  induction n.
  (* case n = 0 *)
  intro m.
  simpl.
  ring.
  (* case n = S n' *)
  intro m.
  simpl.
  rewrite (IHn (m + 2 + n)).
  ring.
Qed.

Lemma wsum_eq_wsigma : forall n, wsigma n 0 = wsum n.
Proof.
  intros.
  rewrite (wsum_m_wsigma n 0).
  reflexivity.
Qed.

End WSumStuff.

(************************** Definitions with more than one recurive call **************************)

Section TreeStuff.

Print list.

Inductive tree (A : Type) : Type :=
| empty : tree A
| node : A -> tree A -> tree A -> tree A.

Arguments empty [A].
Arguments node [A] _ _ _.

Eval compute in (node 3 empty empty).

Fixpoint count {A : Type} (t : tree A) : nat :=
  match t with
    | empty => 0
    | node _ l r => count l + 1 + count r
  end.

Eval compute in count empty.
Eval compute in count (node 3 empty empty).

Fixpoint tcount {A : Type} (t : tree A) (n : nat) : nat :=
  match t with
    | empty => n
    | node _ lt rt => tcount lt (tcount rt (S n))
  end.

Eval compute in tcount empty 0.
Eval compute in tcount (node 3 empty empty) 0.

(* Now, the intermediate lemma *)

Lemma count_m_tcount : forall (A : Type) (t : tree A) m, tcount t m = count t + m.
Proof.
  intro t.
  induction t0.
  (* case t = empty *)
  intro m.
  reflexivity.
  (* case t = node _ l r *)
  intro m.
  simpl.
  rewrite (IHt0_2 (S m)).
  rewrite (IHt0_1 (count t0_2 + S m)).
  ring.
Qed.

(* Goal *)
Lemma count_eq_tcount : forall (A : Type) (t : tree A), tcount t 0 = count t.
Proof.
  intros.
  rewrite (count_m_tcount A t 0).
  ring.
Qed.
  
End TreeStuff.


Section FibStuff.

Fixpoint fib (n : nat) : nat :=
  match n with
    | 0 => 0
    | S n' => match n' with
                | 0 => 1
                | S n'' => fib n' + fib n''
              end
  end.

Function cfib (n a b : nat) : nat :=
  match n with
    | 0 => a
    | S n' => match n' with
                | 0 => b
                | S n'' => cfib n' a b + cfib n'' a b
              end
  end.

Eval compute in fib 10.

Fixpoint tfib (n a b : nat) : nat :=
  match n with
    | 0 => a
    | S n' => tfib n' b (a + b)
  end.

Definition fib' (n : nat) : nat := tfib n 0 1.

Eval compute in fib' 10.

Definition tfib_m (n m : nat) : nat := tfib n m 1.

Goal forall n a b, tfib (S (S n)) a b = tfib (S n) a b + tfib n a b.
intros n a b.
simpl.

Goal forall n a b : nat, tfib n a b = cfib n a b.
Proof.
  intros n a b.
  functional induction (cfib n a b).
  reflexivity.
  reflexivity.
  rewrite <- IHn1. 
  rewrite <- IHn0. 
  clear.


Lemma intermediate_tfib1 : forall n, tfib n 0 0 = 0.
Proof.
  intro n.
  induction n.
  reflexivity.
  simpl.
  rewrite IHn.
  reflexivity.
Qed.  

(* Another intermediate relation probably of no use *)
Lemma intermeidate_tfib1 : forall n m, fib n = tfib (S n) (S m) 1 - tfib (S n) m 1.
Proof. Abort.

Lemma intermediate_fib2 : forall n m, tfib n 0 (S m) = fib n + tfib n 0 m.
Proof.
  intros.
  induction m.
  (* We get stuck in that we need to prove: tfib n 0 1 = fib n *)
  Abort.

Lemma intermediate_fib1 : forall n m, tfib n 0 m = m * fib n.
Proof.
  intro n.
  induction n.
  (* case n = 0 *)
  intro m.
  simpl; ring.
  (* case n = S n' *)
  intro m.
  Restart.

  intros.
  induction m.
  (* case m = 0 *)
  simpl.
  rewrite intermediate_tfib1.
  reflexivity.
  simpl.
  rewrite <- IHm.
  (* Got to prove: tfib n 0 (S m) = fib n + tfib n 0 m *)


Lemma tfib_m_eq : forall n m, tfib_m n m = m + fib n.
Proof.
  intros n.
  unfold tfib_m.
  induction n.
  (* case n = 0 *)
  intro m.
  simpl; ring.
  (* case n = S n' *)
  intro m.
  simpl.
  (*
  We can't use following here.
    Error: Found no subterm matching "tfib n (m + 1) 1" in the current goal.
  In the current goal tfib has a and b positions swapped w.r.t to m.
  So, we need a relationship of some form tying tfib n a b and tfib n b a.
  *)
  (* rewrite (IHn (m + 1)). *)
  Abort.


Eval compute in fib 3.
Eval compute in tfib_m 3 3.

(* This is not the intermediate lemma we require either *)
Lemma tfib_a_b_fib : forall n a b,
                       tfib (S (S n)) a b = fib (S (S n)) + a + b.
Proof.
  intros.
  induction n.
  (* case n = 0 *)
  simpl.
  (* case n = S n *)
  Abort.

Lemma tfib_eq_fib : forall n,
                      fib n = fib' n.
Proof.
  intro n.
  induction n.
  (* case n = 0 *)
  auto.
  (* case n = S n' *)
  Abort.


End FibStuff.