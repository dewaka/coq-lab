(* Omega tactic usage examples *)

Require Import Omega.

Definition square := fun x => x*x.

Eval compute in square 3.

Print square.

Theorem omega_exmaple2: forall x y : nat,
                          0 <= square x -> 3 * (square x) <= 2 * y -> square x <= y.
Proof.
  intros x y H H0; omega.
Qed.