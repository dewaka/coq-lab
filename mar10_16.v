Require Import Ring.
Require Import Vector.
Require Import ArithRing.

(* http://stackoverflow.com/questions/28484450/about-the-refine-tactic-in-coq *)
Section SOverflow1.
  Variable A : Type.
  Variable f g : A -> A.
  Axiom Hfg : forall x, f x = g x.
  Variable a b : A.
  Axiom t : g a = g b.

  Goal f a = g b.
  Proof.
    Print eq_trans.

    (* one way *)
    (* rewrite <- t. *)
    (* refine (eq_trans (Hfg a) _). *)
    (* auto. *)

    (* another way *)
    (* refine (eq_trans (Hfg _) _); apply t. *)

    (* simplest *)
    (* refine (eq_trans (Hfg _) t). *)

    (* another way. clearest for me  *)
    rewrite Hfg.
    rewrite t.
    reflexivity.

    (* using hints *)
    (* Hint Rewrite Hfg t : my_eqs. *)
    (* autorewrite with my_eqs. *)
    (* reflexivity. *)

    (* using congruence tactic *)
    (* I do not know how congruence works *)
    (* generalize Hfg t; intros. *)
    (* congruence. *)
  Qed.
End SOverflow1.

Section PC.
  Variables P Q : nat -> Prop.
  Variable R : nat -> nat -> Prop.

  Lemma PQR : forall x y : nat,
                (R x x -> P x -> Q x) -> P x -> R x y -> Q x.
  Proof.
  Abort.
    

End PC.

(* Fixpoint *)
(*   ScatHUnion_0 {A} (n : nat) (pad : nat) : t A n -> t (option A) ((S pad) * n). *)
(*   refine ( *)


(* Fixpoint *)
(*   ScatHUnion_0 {A} (n:nat) (pad:nat) : t A n -> t (option A) ((S pad) * n). *)
(*  refine ( *)
(*   match n return (t A n) -> (t (option A) ((S pad)*n)) with *)
(*   | 0 => fun _ =>  (fun H => _)(@nil (option A)) *)
(*   | S p => *)
(*     fun a => *)
(*       let foo := (@ScatHUnion_0 A p pad (tl a)) in *)
(*       (fun H => _) (cons _ (Some (hd a)) _ (append (const None pad) foo)) *)
(*   end *)
(*    ). *)
(*  rewrite  <-(mult_n_O (S pad)); auto. *)
(*  replace  (S pad * S p) with ( (S (pad + S pad * p)) ); auto; ring. *)
(* Defined. *)

(* Came across this interesting question in StackOverflow -
   http://stackoverflow.com/questions/30683347/why-can-i-sometimes-prove-a-goal-via-a-lemma-but-not-directly *)