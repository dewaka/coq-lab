Fixpoint fact (n : nat) :=
  match n with
    | 0 => 1
    | S n' => n * fact n'
  end.

Fixpoint tfact (n m : nat) :=
  match n with
    | 0 => m
    | S n' => tfact n' (m * n)
  end.
          
Fixpoint fact' (n : nat) := tfact n 1.

Eval compute in fact 3.
Eval compute in fact' 3.

Fixpoint plus' n m : nat :=
  match m with
    | 0 => n
    | S m' => S (plus' n m')
  end.

Fixpoint tail_plus n m : nat :=
  match n with
    | O => m
    | S n => tail_plus n (S m)
  end.

Lemma plus_tail_plus_orig : forall n m, n + m = tail_plus n m.
unfold tail_plus in |- *; induction n as [| n IHn]; simpl in |- *; auto.
intro m; rewrite <- IHn; simpl in |- *; auto.
Qed.

Lemma plus_tail_plus_simple1 : forall (n m : nat),
                            n + m = tail_plus n m.
Proof.
  unfold tail_plus in |- *.
  induction n as [| n IHn].
  simpl in |- *.
  auto.
  intro m.  
  rewrite <- IHn.
  simpl in |- *.
  auto.
Qed.

Lemma plus_tail_plus_simple2 : forall (n m : nat),
                            n + m = tail_plus n m.
Proof.
  unfold tail_plus.
  induction n.
  simpl.
  auto.
  intro m.
  rewrite <- IHn.
  simpl.
  auto.
Qed.

Print plus'.
Print Nat.add.

Lemma plus'_plus : forall (n m : nat),
                     plus' n m = n + m.
Proof.
  intros.
  induction m.
  auto.
  simpl.
  rewrite IHm.
  auto.
Qed.

Lemma plus'_tail_plus : forall (n m : nat),
                          plus' n m = tail_plus n m.
Proof.
  intros.
  rewrite plus'_plus.
  apply plus_tail_plus_simple2.
Qed.

Lemma factor_mul : forall (n m : nat),
                     m + n * m = m * (1 + n).
Proof.

  (* SearchAbout mul_dist. *)
  (* intros. *)
  (* induction m. *)
  (* simpl. *)
  (* auto. *)
  (* simpl. *)
  (* simpl in IHm. *)
  (* rewrite <- IHm. *)
  (* simpl. *)
  (* Abort. *)
    

(* This is what we'd like to prove *)
Lemma fact_eq_tail_fact : forall (n : nat),
                            fact n = tfact n 1.
Proof.
  unfold tfact.
  induction n.
  auto.
  simpl.
  (* rewrite IHn. *)
  rewrite IHn
Abort.

Fixpoint wsum (n : nat) :=
  match n with
    | 0 => 0
    | S n' => S n + wsum n'
  end.

Eval compute in wsum 5.         (* 20 *)

Fixpoint wsigma (n y : nat) :=
  match n with
    | 0 => y
    | S n' => wsigma n' (y + S (S 0) + n')
  end.

Definition wsum' (n : nat) := wsigma n 0.

Eval compute in wsum' 5.        (* 20 *)

(* Now to prove this *)
Lemma wsum_eq_wsum' : forall (n : nat),
                        wsum n = wsum' n.
Proof.
Abort.