Require Import Ring.
Require Import Omega.

Fixpoint plus' n m : nat :=
  match m with
    | 0 => n
    | S m' => S (plus' n m')
  end.

Fixpoint tail_plus n m : nat :=
  match n with
    | O => m
    | S n => tail_plus n (S m)
  end.

Lemma plus_tail_plus_simple2 : forall (n m : nat),
                            n + m = tail_plus n m.
Proof.
  unfold tail_plus.
  induction n.
  simpl.
  auto.
  intro m.
  rewrite <- IHn.
  simpl.
  auto.
Qed.

Lemma plus_tail_plus_simple3 : forall (n m : nat),
                                 n + m = tail_plus n m.
Proof.
  unfold tail_plus.
  induction n.
  (* case n = 0 *)
  reflexivity.
  (* case n = S n *)
  intro m.
  rewrite <- IHn.
  simpl.
  auto.
Qed.


Fixpoint fact (n : nat) :=
  match n with
    | 0 => 1
    | S n' => n * fact n'
  end.

Fixpoint tfact (n m : nat) :=
  match n with
    | 0 => m
    | S n' => tfact n' (m * n)
  end.

(*
fact (n+1) = fact n * (1 + n)
*)

SearchAbout (_ * _ + _ = (_ + 1) * _).

Lemma factor1' : forall a b,
                  a + a * b = a * (1 + b).
Proof.
  intros a b.
  SearchAbout (_ * (_ + _) = (_ * _ + _ * _)).
  rewrite Nat.mul_add_distr_l.
  SearchAbout (_ * 1 = _).
  rewrite Nat.mul_1_r.
  reflexivity.
Qed.

Lemma factor1 : forall a b,
                  a + a * b = a * (1 + b).
Proof.
  intros.
  induction a.
  (* a = 0 *)
  reflexivity.
  (* a = S n *)
  simpl.
  rewrite plus_assoc.
  rewrite plus_comm.
  rewrite plus_assoc.
  rewrite plus_comm in IHa.
  rewrite IHa.
  simpl.
  rewrite plus_comm.
  reflexivity.
Qed.

Lemma fact_n_plus_1 : forall n,
                        fact (n + 1) = fact n * (n + 1).
Proof.
  intro n.
  unfold fact.
  induction n.
  (* case n = 0 *)
  reflexivity.
  (* case n = S n' *)
  simpl.
  rewrite IHn.
  fold fact.
  fold fact in IHn.
  rewrite <- IHn.
  rewrite mult_comm.
  rewrite factor1.
  simpl.
  rewrite IHn.
  rewrite plus_comm.
  rewrite factor1.
  

  (* intro n. *)
  (* induction n. *)
  (* (* case n = 0 *) *)
  (* reflexivity. *)
  (* (* case n = 1 *) *)
  (* simpl. *)
  (* rewrite IHn *)
  (* simpl *)
  

(*
          
Fixpoint fact' (n : nat) := tfact n 1.
Lemma fact_eq_tail_fact : forall (n : nat),
                            fact n = tfact n 1.
Proof.
  unfold tfact.
  induction n.
  (* case n = 0 *)
  reflexivity.
  (* case n = S n *)
  simpl.

*) 