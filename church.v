(* https://en.wikipedia.org/wiki/Church_encoding *)

Require Import Arith.
Require Import List.

Module Church.

  Definition cnat := forall X : Type, (X -> X) -> X -> X.

  Definition zero : cnat :=
    fun (X : Type) (f : X -> X) (x : X) => x.

  Definition one : cnat :=
    fun (X : Type) (f : X -> X) (x : X) => f x.

  Definition two : cnat :=
    fun (X : Type) (f : X -> X) (x : X) => f (f x).

  Definition succ (n : cnat) : cnat :=
    fun (X : Type) (f : X -> X) (x : X) => f (n X f x).

  Example succ_1 : succ zero = one.
  Proof.
    reflexivity.
  Qed.

  Definition plus (m n : cnat) : cnat :=
    fun (X : Type) (f : X -> X) (x : X) => m X f (n X f x).

  Example plus_1 : plus zero one = one.
  Proof.
    reflexivity.
  Qed.

  Definition succ' := plus one.

  Example succ'_1 : succ' zero = one.
  Proof. reflexivity. Qed.

  (* f^(m*n) x = (f^n)^m x *)
  Definition mult (m n : cnat) : cnat :=
    fun (X : Type) (f : X -> X) => m X (n X f).
    
  Example mult_2_2_is_4 : mult two two = plus two two.
  Proof.
    reflexivity.
  Qed.


  (* Definition exp {X : Type} (m n : cnat) := *)
  (*   m X (n X). *)

End Church.

Lemma silly3_firsttry : forall (n : nat),
                          true = beq_nat n 5 ->
                          beq_nat (S (S n)) 7 = true.
Proof.
  auto.
Qed.

Theorem rev_ex1 : forall (l l' : list nat),
                    l = rev l' ->
                    l' = rev l.
Proof.
  intros.
  rewrite H.
  symmetry.                     (* Quite nice to turn things arounjd *)
  SearchAbout rev (rev _).
  apply rev_involutive.
Qed.

