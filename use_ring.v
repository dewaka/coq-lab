(* The ring tactic *)

Require Import Ring.
Require Import ZArith.
Require Import Arith.


Section RingExamples.
Open Scope Z_scope.

Theorem ring_example1: forall x y : Z,
                         (x + y)*(x + y) = x * x + 2 * x * y + y * y.
Proof.
  intros x y; ring.
Qed.

Check ring_example1.

Definition square (z : Z) := z * z.

Theorem ring_example2: forall x y : Z,
                         square (x+y) = square x + 2*x*y + square y.
Proof.
  intros x y.
  unfold square; ring.
Qed.

Theorem ring_exmple4: (forall x : nat, (S x)*(x + 1) = x * x + (x + x + 1))%nat.
Proof.
Abort. 

End RingExamples.

Section NatFormula.

Open Scope nat.

(* Sum of first n natural number *)
Fixpoint sum_n (n : nat) : nat :=
  match n with
    | 0 => 0
    | S n' => n' + sum_n n'
  end.

Eval compute in sum_n 10.

Lemma sum_n_p : forall n, 2 * sum_n n + n = n * n.
Proof.
  intro n.
  induction n.
  (* case n = 0 *)
  reflexivity.
  (* case n = S n' *)
  assert (SnSn : S n * S n = n * n + 2 * n + 1).
  ring.
  rewrite SnSn.
  rewrite <- IHn.
  simpl.
  ring.
Qed.

Fixpoint sum_squares_n (n : nat) : nat :=
  match n with
    | 0 => 0
    | S n' => n * n + sum_squares_n n'
  end.

Definition sum_squares_val (n : nat) : nat := n * (n + 1) * (2 * n + 1).

Eval compute in (sum_squares_n 10) * 6.
Eval compute in sum_squares_val 10.

Eval compute in (sum_squares_n 1) * 6.
Eval compute in sum_squares_val 1.

(*** Paper Proof ***********************************************************

By definition (1):
  sum_squares_n (n+1) = sum_squares_n n + (n+1)*(n+1)
Induction hypothesis (2):
  6 * sum_squares_n n = n * (n + 1) * (2 * n + 1)
=> LHS = 2 n^3+3 n^2+n

To prove (3):
  6 * sum_squares_n (n+1) = (n+1) * (n+2) * (2*(n+1) + 1)
  RHS = 2 n^3+9 n^2+13 n+6

Proof:
  Using (1) on (3):
  LHS = 6 * (sum_squares_n n + (n+1)*(n+1))
      = 6 * sum_squares_n n + 6 (n+1)^2
      Using (2):
      = 2 n^3 + 3 n^2+n + 6 (n+1)^2
      = 2 n^3 + 3 n^2+n + 6 (n^2 + 2 n + 1)
      = 2 n^3+9 n^2+13 n+6
      = RHS
  QED

*** How to do the same in Coq is the next question **************************)

(* Prove: sum_squares_n (n+1) = sum_squares_n n + (n+1)*(n+1) *)
Lemma sum_squares_lem1 : forall n, sum_squares_n (S n) = sum_squares_n n + (S n) * (S n).
Proof.
  intro n.
  rewrite plus_comm.
  auto.
Qed.  

Lemma sum_squares_ident : forall n, 6 * sum_squares_n n = sum_squares_val n.
Proof.
  intros n.
  induction n.
  (* case n = 0 *)
  auto.
  (* case n = S n' *)
  unfold sum_squares_val.
  unfold sum_squares_val in IHn.
  (* assert (sum_sq : sum_squares_n (S n) = sum_squares_n n + (n + 1)*(n + 1)). *)
  rewrite sum_squares_lem1.
  (* SearchAbout (_ * (_ + _)). *)
  rewrite Nat.mul_add_distr_l.
  rewrite IHn.
  ring.
Qed.

End NatFormula.