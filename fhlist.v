Require Import List.
Require Import Bool.
Require Import Arith.
Import ListNotations.

Section fhlist.
  Variable A : Type.
  Variable B : A -> Type.

  Fixpoint fhlist (ls : list A) : Type :=
    match ls with
      | nil => unit
      | x :: ls' => B x * fhlist ls'
    end%type.

  Variable elm : A.

  Fixpoint fmember (ls : list A) : Type :=
    match ls with
      | nil => Empty_set
      | x :: ls' => (x = elm) + fmember ls'
    end%type.

  Fixpoint fhget (ls : list A) : fhlist ls -> fmember ls -> B elm :=
    match ls return fhlist ls -> fmember ls -> B elm with
      | nil => fun _ idx => match idx with end
      | _ :: ls' => fun mls idx =>
                      match idx with
                        | inl pf => match pf with
                                      | eq_refl => fst mls
                                    end
                        | inr idx' => fhget ls' (snd mls) idx'
                      end
    end.

End fhlist.

Implicit Arguments fhget [A B elm ls].

Section fhlist_map.
  Variable A : Type.
  Variable B C : A -> Type.
  Variable f : forall x, B x -> C x.

  Print f.                      (*   [f : forall x : A, B x -> C x] *)
  
  Fixpoint fhmap (ls : list A) : fhlist (B  ls) -> fhlist (C ls) :=
    match ls return fhlist B ls -> fhlist C ls with
      | _ => fun _ => tt
    end.