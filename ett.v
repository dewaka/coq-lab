(* Definition x := x.              (* This does not type check *) *)

(* Require Import Mult. *)
(* Require Import Plus. *)
Require Import Nat.
Require Import Plus.
Require Import Mult.
Require Import Arith.

(* Search Nat.mul. *)
(* Print mult_plus_distr_r. *)
(* Search "mul_add". *)
(* Search "mul_add". *)
(* Search "mul_add_distr_l". *)

Print Nat.

Search "mul_add".


Lemma factor_mul : forall (n m : nat),
                     m + m * n = m * (1 + n).
Proof.
  intros.
  SearchPattern (_ * (_ + _) = _ + _).
  rewrite Nat.mul_add_distr_l.
  SearchPattern (_ * 1 = _).
  rewrite Nat.mul_1_r.
  auto.
Qed.

(* Yeah! *)
SearchAbout (_ + _ * _ = _ * (1 + _)).

Lemma factor_mul' : forall n m,
                      m + n * m = m * (n + 1).
Proof.
  intros n m.
  SearchPattern (_ * (_ + _) = _ + _).
  rewrite Nat.mul_add_distr_l.
  rewrite Nat.mul_1_r.
  SearchAbout (_ * _ = _ * _).
  rewrite Nat.mul_comm.
  rewrite Nat.add_comm.
  auto.
Qed.

Fixpoint fact (n : nat) :=
  match n with
    | 0 => 1
    | S n' => n * fact n'
  end.

Fixpoint tfact (n m : nat) :=
  match n with
    | 0 => m
    | S n' => tfact n' (m * n)
  end.
          
Fixpoint fact' (n : nat) := tfact n 1.

Eval compute in fact 5.
Eval compute in fact' 5.       (* Even the tail recursive one cannot do a fact' 10! Stack overflows! *)

Fixpoint plus' n m : nat :=
  match m with
    | 0 => n
    | S m' => S (plus' n m')
  end.

Fixpoint tail_plus n m : nat :=
  match n with
    | O => m
    | S n => tail_plus n (S m)
  end.

Lemma plus_and_tail_plus : forall n m,
                             n + m = tail_plus n m.
Proof.
  unfold tail_plus.
  induction n.
  (* case n = 0 *)
  reflexivity.
  (* case n = S n *)
  induction m.
  


Lemma fact_eq_tail_fact : forall (n : nat),
                            fact n = tfact n 1.
Proof.
Abort.