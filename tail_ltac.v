Require Import Arith.
Require Import List.

Import ListNotations.

(* Ltac tactic definitions for tail recursive equivalence proofs *)

Print le_n.
Print le_S.

(* Inductive le (n : nat) : nat -> Prop := *)
(*     le_n : n <= n | le_S : forall m : nat, n <= m -> n <= S m *)

(* Simple recursive tactic *)
Ltac le_S_star := apply le_n || (apply le_S; le_S_star).

Theorem le_5_25 : 5 <= 25.
Proof.
  le_S_star.
  Show Proof.   (* this is pretty cool. dumps the proof with expanded tactics *)
Qed.

Print not.
(*
Something to think about
http://stackoverflow.com/questions/23996056/elim-a-double-negation-hypothesis-in-coq-proof-assistant
*)
(* Fact not_le_25_5 : not (25 <= 5). *)
(* Proof. *)
(*   unfold not. *)

Ltac contrapose H :=
  match goal with
    | [id:(~_) |- (~_) ] => intro H; apply id
  end.

Fact example_contrapose : forall x y : nat, x <> y -> x <= y -> ~y <= x.
Proof.
  intros x y H H0.
  contrapose H'.
  auto with arith.
  Show Proof.
  (* (fun (x y : nat) (H : x <> y) (H0 : x <= y) (H' : y <= x) => *)
  (*  H (Nat.le_antisymm x y H0 H')) *)
Qed.

(* Primes example from Coq'art book *)

Section Primes.

Definition divides (n m : nat) := exists p : nat, p*n = m.

Hypothesis
  (divides_0 : forall n : nat, divides n 0)
  (divides_plus : forall n m : nat, divides n m -> divides n (n+m))
  (not_divides_plus : forall n m : nat, ~divides n m -> ~divides n (n+m))
  (not_divides_lt : forall n m : nat, 0 < m -> m < n -> ~divides n m)
  (not_lt_2_divides :
     forall n m : nat, n <> 1 -> n < 2 -> 0 < m -> ~divides n m)
  (le_plus_minus : forall n m : nat, le n m -> m = n+(m-n))
  (lt_lt_or_eq : forall n m : nat, n < S m -> n < m \/ n = m).

(* Tactic to check whether division holds or not *)
Ltac check_not_divides :=
  match goal with
    | [ |- (~divides ?X1 ?X2) ] =>
      cut (X1<=X2); [ idtac | le_S_star ]; intros Hle;
      rewrite (le_plus_minus _ _ Hle); apply not_divides_plus;
      simpl; clear Hle; check_not_divides
    | [ |- _] =>
      apply not_divides_lt; unfold lt; le_S_star
  end.

Ltac check_lt_not_divides :=
  match goal with
    | [Hlt:(lt ?X1 2%nat) |- (~divides ?X1 ?X2) ] =>
      apply not_lt_2_divides; auto
    | [Hlt:(lt ?X1 ?X2) |- (~divides ?X1 ?X3) ] =>
      elim (lt_lt_or_eq _ _ Hlt);
        [clear Hlt; intros Hlt; check_lt_not_divides
        | intros Heq; rewrite Heq; check_not_divides]
  end.

Definition is_prime : nat -> Prop :=
  fun p : nat => forall n : nat, n <> 1 -> lt n p -> ~divides n p.

Fact prime13 : is_prime 13.
Proof.
  Time (unfold is_prime; intros; check_lt_not_divides).
  (* Show Proof. *)
  auto with arith.
Qed.

Fact prime3 : is_prime 3.
Proof.
  unfold is_prime.
  intros.
  elim (lt_lt_or_eq _ _ H0).
  intros.
  elim (lt_lt_or_eq _ _ H1).
  intros.
  elim (lt_lt_or_eq _ _ H2).
  intros.

  auto with arith.
  Abort.
  

Fact prime61 : is_prime 61.
Proof.
  unfold is_prime; intros; check_lt_not_divides.
  auto with arith.
Qed.

Fact prime21 : is_prime 21.
Proof.
  (* This does not work as expected *)
  (* unfold is_prime; intros; check_lt_not_divides. *)
Abort.



End Primes.

Section ConditionalLtac.

Theorem S_to_plus_one : forall n : nat, S n = n + 1.
Proof. intros; rewrite plus_comm; reflexivity. Qed.

Ltac S_to_plus_simpl :=
  match goal with
    | [ |- context [(S ?X1)] ] =>
      match X1 with
        | O%nat => fail 1
        | ?X2 => rewrite (S_to_plus_one X2); S_to_plus_simpl
      end
    | [ |- _ ] => idtac
  end.

Theorem ring_example7 : forall n m : nat, n * O + (S n) * m = n * n * O + m * n + m.
Proof.    
  intros.
  (* The following lines are not even required. Has Coq improved its ring tactic since Coq'art *)
  (* assert (Sp1 : S n = n + 1). *)
  (* rewrite plus_comm; reflexivity. *)
  (* rewrite Sp1. *)
  ring.
Qed.

Theorem ring_example7' : forall n m : nat, n * O + (S n) * m = n * n * O + m * n + m.
Proof.
  intros.
  S_to_plus_simpl. (* This step is not required, but shows how to use above tactic *)
  ring.
Qed.
  
End ConditionalLtac.

Section ReductionTac.

Ltac simple_on e :=
  let v := eval simpl in e
  in match goal with
       | [ |- context [e] ] => replace e with v; [idtac | auto]
     end.


(* Theorem simple_on_example : forall n : nat, exists m : nat, (1+n) + 4*(1+n) = 5*(S m). *)
(* Proof. *)
(*   intros n. *)
(*   simple_on (1 + n). *)
(*   SearchAbout (_ + _ * _). *)

End ReductionTac.

Section TailTactic.
  
(*
Tail recursive proofs follow a pattern as follows. Rewrites based on the
induction hypothesis is a common theme here.

How many rewrites depend on the shape of the function. If it is single recurive
function then there will only be one rewrite as in the following example.

Lemma tfact_m_fact : forall n m,
                       tfact n m = m * fact n.
Proof.
  intro n.
  induction n.
  intro m.
  simpl.
  ring.
  intro m.
  simpl.
  rewrite (IHn (m * S n)).
  ring.
Qed.

Same pattern in the following.

Lemma wsum_m_wsigma : forall n m, wsigma n m = m + wsum n.
Proof.
  intro n.
  induction n.
  (* case n = 0 *)
  intro m.
  simpl.
  ring.
  (* case n = S n' *)
  intro m.
  simpl.
  rewrite (IHn (m + 2 + n)).
  ring.
Qed.

As following example demonstrates, if the recursion in question is double
recursive there will be two rewrites in the proof.

Lemma count_m_tcount : forall (A : Type) (t : tree A) m, tcount t m = count t + m.
Proof.
  intro t.
  induction t0.
  (* case t = empty *)
  intro m.
  reflexivity.
  (* case t = node _ l r *)
  intro m.
  simpl.
  rewrite (IHt0_2 (S m)).
  rewrite (IHt0_1 (count t0_2 + S m)).
  ring.
Qed.

In each case how we use the induction hypothesis to rewrite the goal is
different and that's something which might have to be supplied by the user of
the tactic.

*)

(* Fact example again to go through with a Ltac in mind to do the most of the work *)
Fixpoint fact (n : nat) :=
  match n with
    | 0 => 1
    | S n' => n * fact n'
  end.

Fixpoint tfact (n m : nat) :=
  match n with
    | 0 => m
    | S n' => tfact n' (m * n)
  end.

Require Import Omega.

Ltac rewrite_inductive :=
  match goal with
    | [ H : forall n, ?P ?x n = _ |- ?P ?x ?m = _ ] => rewrite (H m)
    (* | [ H : forall n, ?P ?x n = _ |- ?P ?x ?m = _ ] => rewrite (H m) *)
    | _ => idtac
  end.

Lemma tfact_m_fact : forall n m,
                       tfact n m = m * fact n.
Proof.
  intro.
  induction n.
  (* case n = 0 *)
  intro m.
  simpl; ring.
  (* case n = S n *)
  intro m.
  simpl.
  (* Here we need a tactic to identify that we can rewrite it using the induction hypothesis
     specifically using the argument in the current goal's accumulator position.

   tfact n (m * S n) = m * (fact n + n * fact n)
   --------^^^^^^^^^----------------------------
   *)
  (* rewrite (IHn (m * S n)). *)
  (*
     IHn -> induction hypothesis
     (m * S n) -> the arg in accumulator position
     So, the important point is that we can get from the context the arg to instantiate IHn.
  *)
  (* David developed this inline as shown below and I moved that to a reusable tactic *)
  (* match goal with *)
  (*   | [ H : forall n, ?P ?x n = _ |- ?P ?x ?m = _ ] => rewrite (H m) *)
  (* end. *)
  rewrite_inductive.
  ring.
Qed.

Fixpoint wsum (n : nat) : nat :=
  match n with
    | 0 => 0
    | S n' => S n + wsum n'
  end.

Fixpoint wsigma (n y : nat) : nat :=
  match n with
    | 0 => y
    | S n' => wsigma n' (y + (S (S 0)) + n')
  end.

(* Place holder for now *)
Ltac hl_trec :=
  match goal with | _ => idtac end.

Lemma wsum_m_wsigma : forall n m, wsigma n m = m + wsum n.
Proof.
  intro n.
  induction n.
  (* case n = 0 *)
  intro m.
  simpl; ring.
  (* case n = S n' *)
  intro m.
  simpl.
  rewrite_inductive.            (* we can use rewrite_inductive here *)
  ring.
Qed.

Fixpoint reverse {A} (l : list A) : list A :=
  match l with
    | [] => []
    | x :: xs => reverse xs ++ [x]
  end.

Fixpoint treverse {A} (l : list A) (acc : list A) : list A :=
  match l with
    | [] => acc
    | x :: xs => treverse xs (x :: acc)
  end.

Definition reverse' {A} (l : list A) := treverse l [].

Lemma treverse_m_reverse : forall A (l : list A) (m : list A),
                             treverse l m =  reverse l ++ m.
Proof.
  induction l.
  (* l = [] *)
  reflexivity.
  (* l = cons _ l' *)
  intro m.
  simpl.
  rewrite_inductive.            (* Yes, we can use this here as well. Pretty useful *)
  rewrite <- app_assoc.
  reflexivity.
Qed.

End TailTactic.

