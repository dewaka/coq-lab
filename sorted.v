(* http://www.randomhacks.net/2015/07/19/proving-sorted-lists-correct-using-coq-proof-assistent/ *)

Require Import List.
Require Import Arith.
Require Import Sorted.
Import ListNotations.

(* http://www.cs.princeton.edu/courses/archive/fall09/cos441/sf/SfLib.html *)
Fixpoint ble_nat (n m : nat) {struct n} : bool :=
  match n with
    | O => true
    | S n' => match m with
                | O => false
                | S m' => ble_nat n' m'
              end
  end.

Fixpoint insert_sorted (n : nat) (ls : list nat) : list nat :=
  match ls with
    | [] => [n]
    | x :: xs => if ble_nat n x
                 then n :: ls
                 else x :: insert_sorted n xs
  end.

Example test_insert_3_1_2 :
  insert_sorted 2 (insert_sorted 1 (insert_sorted 3 [])) = [1; 2; 3].
Proof. reflexivity. Qed.

Eval compute in leb 3 4.

(* Theorem insert_sorted_stays_sorted : forall n l, *)
(*    Sorted leb l -> Sorted leb (insert_sorted n l). *)