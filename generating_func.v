(* Exploring some generating functions from generatingfunctionology book *)

(* (A) a(n+1) = 2a(n) + 1, for n >= 0 AND a0 = 0 *)
Fixpoint series_2an_plus_1 (n : nat) : nat :=
  match n with
    | O => O
    | S n' => S (2 * series_2an_plus_1 n')
  end.

Eval compute in series_2an_plus_1 3.

(*
We can prove using a recurrance formula that (A) is equal to 2^n-1.
This should be a simple induction proof based on the recurrance relation.
*)