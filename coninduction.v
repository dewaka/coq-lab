Inductive list (A : Set) : Set :=
| nil : list A
| cons : A -> list A -> list A.

CoInductive LList (A : Set) : Set :=
| LNil : LList A
| LCons : A -> LList A -> LList A.

Implicit Arguments LNil [A].

CoInductive LTree (A : Set) : Set :=
| LLeaf : LTree A
| LBin : A -> LTree A -> LTree A -> LTree A.

Implicit Arguments LLeaf [A].

Definition isEmpty {A : Set} (l : LList A) : Prop :=
  match l with
    | LNil => True
    | LCons _ _ _ => False
  end.

Definition LHead {A : Set} (l : LList A) : option A :=
  match l with
    | LNil => None
    | LCons _ a t => Some a
  end.

Definition LTail {A : Set} (l : LList A) : LList A :=
  match l with
    | LNil => LNil
    | LCons _ a t => t
  end.

Fixpoint LNth {A : Set} (n : nat) (l : LList A) {struct n} : option A :=
  match n with
    | 0 => match l with
             | LNil => None
             | LCons _ a t => Some a
           end
    | S n' => match l with
                | LNil => None
                | LCons _ a t => LNth n' t
              end
  end.

(* This is the same as above definition, but simpler *)
Fixpoint LNth' {A : Set} (n : nat) (l : LList A) {struct n} : option A :=
  match l with
    | LNil => None
    | LCons _ a t => match n with
                       | 0 => Some a
                       | S n' => LNth' n' t
                     end
  end.

(* Build a list of natural numbers with increasing order *)
CoFixpoint from (n : nat) : LList nat :=
  LCons _ n (from (S n)).

Check from 3.

Definition Nats := from 0.      (* Natural numbers *)

Definition squares_from :=
  let sqr := fun n : nat => n*n in
  cofix F : nat -> LList nat :=
  fun n : nat => LCons _ (sqr n) (F (S n)).

Eval simpl in isEmpty Nats.

Eval simpl in from 3.
Eval compute in from 3.

Print from.

(* LAppend function *)
CoFixpoint LAppend {A : Set} (l1 : LList A) (l2 : LList A) : LList A :=
  match l1 with
    | LNil => l2
    | LCons _ x l1' => LCons _ x (LAppend l1' l2)
  end.

CoFixpoint general_omega {A : Set} (u v : LList A) : LList A :=
  match u with
    | LNil => v
    | LCons _ x u' => match v with
                        | LNil => LCons _ x (general_omega u' u)
                        | LCons _ y v' => LCons _ y (general_omega v' u)
                      end
  end.
 
Definition omega {A : Set} (u : LList A) : LList A := general_omega u u.

Print omega.