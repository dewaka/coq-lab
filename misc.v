Require Import List.
Require Import Bool.
Require Import Arith.
Import ListNotations.

Theorem silly1 : forall (n m o p : nat),
                   n = m ->
                   [n; o] = [n; p] ->
                   [n; o] = [m; p].
Proof.
  intros.
  rewrite H0.
  rewrite H.
  reflexivity.
Qed.

(* Another way to prove above *)
Theorem silly1' : forall (n m o p : nat),
                   n = m ->
                   [n; o] = [n; p] ->
                   [n; o] = [m; p].
Proof.
  intros n m o p eq1 eq2.
  rewrite <- eq1.
  apply eq2.
Qed.

Theorem silly2 : forall n m o p : nat,
                   n = m ->
                   (forall (q r : nat), q = r -> [q; o] = [r; p]) ->
                   [n; o] = [m; p].
Proof.
  intros n m o p eq1 eq2.
  apply eq2.
  apply eq1.
Qed.

Theorem silly2a : forall n m : nat,
                    (n, n) = (m, m) ->
                    (forall q r : nat, (q, q) = (r, r) -> [q] = [r]) ->
                    [n] = [m].
Proof.
  intros n m eq1 eq2.
  apply eq2.
  apply eq1.
Qed.

Fixpoint evenb (n : nat) : bool :=
  match n with
    | 0 => true
    | (S n) => oddb n
  end
with oddb (n : nat) : bool :=
  match n with
    | 0 => false
    | (S n) => evenb n
  end.

Theorem silly_ex :
  (forall n, evenb n = true -> oddb (S n) = true) ->
  evenb 3 = true ->
  oddb 4 = true.
Proof.
  intros H1 H2.
  apply H2.
Qed.

Theorem silly3 : forall (n : nat),
                   true = beq_nat n 5 ->
                   beq_nat (S (S n)) 7 = true.
Proof.
  intros n H.
  simpl.
  symmetry.
  apply H.
Qed.

Theorem silly3' : forall (n : nat),
                   true = beq_nat n 5 ->
                   beq_nat (S (S n)) 7 = true.
Proof.
  intros n H.
  symmetry.
  apply H.                      (* This illustrates that simpl is not necessary before apply tactic because it will do the simpl anyway *)
Qed.

Eval compute in rev [1; 2; 3].

Theorem rev_eq : forall (A : Type) (l l' : list A),
                   l = l' -> rev l = rev l'.
Proof.
  intros.
  induction l.
  simpl.
  rewrite <- H.
  reflexivity.
  rewrite H.
  reflexivity.
Qed.

Theorem rev_rev_eq : forall (A : Type) (l l' : list A),
                   rev l = rev l' -> l = l'.
Proof.
Abort.

Theorem rev_ex1 : forall (l l' : list nat),
                    l = rev l' ->
                    l' = rev l.
Proof.
  intros l l' H.
  rewrite H.
  SearchAbout rev (rev _).      (* to find rev_involutive *)
  symmetry.
  apply rev_involutive.
Qed.

Example trans_eq_ex : forall (a b c d e f : nat),
                        [a; b] = [c; d] ->
                        [c; d] = [e; f] ->
                        [a; b] = [e; f].
Proof.
  intros.
  rewrite H.
  rewrite H0.
  reflexivity.
Qed.

Theorem trans_eq : forall (X : Type) (n m o : X),
                     n = m -> m = o -> n = o.
Proof.
  intros.
  rewrite H.
  rewrite H0.
  reflexivity.
Qed.

(* We can use the above common trans_eq pattern with apply with tactic pattern *)
Example trans_eq_ex' : forall (a b c d e f : nat),
                        [a; b] = [c; d] ->
                        [c; d] = [e; f] ->
                        [a; b] = [e; f].
Proof.
  intros.
  apply trans_eq with (m := [c; d]).
  apply H.
  apply H0.
Qed.

Definition minustwo (n : nat) := n - 2.

Example trans_eq_ex1 : forall (n m o p : nat),
                         m = (minustwo o) ->
                         (n + p) = m ->
                         (n + p) = (minustwo o).
Proof.
  intros.
  apply trans_eq with (m := m).
  apply H0.
  apply H.
Qed.

SearchAbout eq_add_S.

Theorem eq_add_S : forall (n m : nat),
                     S n = S m ->
                     n = m.
Proof.
  (* this one can be done by just auto! *)
  intros n m eq.
  inversion eq.
  reflexivity.
Qed.

Theorem silly4 : forall n m : nat,
                   [n] = [m] -> n = m.
Proof.
  intros n m eq.
  inversion eq.
  reflexivity.
Qed.

Example sillyex1 : forall (X : Type) (x y z : X) (l j : list X),
                     x :: y :: l = z :: j ->
                     y :: l = x :: j ->
                     x = y.
Proof.
  intros.
  inversion H0.
  reflexivity.
Qed.

Theorem f_equal : forall (A B : Type) (f : A -> B) (x y : A),
                    x = y -> f x = f y.
Proof.
  intros.
  inversion H.
  reflexivity.
Qed.

Theorem beq_nat_0_l : forall n,
                        beq_nat 0 n = true -> n = 0.
Proof.
  intros.
  SearchAbout beq_nat.
  (* Case "0" *)
  destruct n.
  reflexivity.
  (* Case "S n" *)
  inversion H.
Qed.

Theorem beq_nat_0_r : forall n,
                        beq_nat n 0 = true -> n = 0.
Proof.
  intros.
  destruct n.
  (* Case 0 *)
  reflexivity.
  (* Case S n *)
  inversion H.
Qed.
  
Theorem S_inj : forall (n m : nat) (b : bool),
                  beq_nat (S n) (S m) = b ->
                  beq_nat n m = b.
Proof.
  intros.
  simpl in H.
  apply H.
Qed.

Definition double n := n + n.

(*
Theorem double_injective : forall n m, double n = double m -> n = m.
Proof.
  intros n m H.
  induction n.
  (* Case n=0 *)
  induction m.
  (* Case m=0 *)
  reflexivity.
  (* Case m=S p *)
  simpl.
  inversion H.
 *) 

Theorem double_injective : forall n m, double n = double m -> n = m.
Proof.
  intros n.
  induction n as [| n'].

  (* Case n = 0 *)
  simpl. intros m eq. destruct m as [| m'].
    (* Case m = 0 *) reflexivity.
    (* Case m = S m *) inversion eq.

  (* Case n = S n *)
     intros m eq.
     destruct m as [| m'].
     (* Case m = 0 *) inversion eq.

     (* Case m = S m *)
     Abort.

(* Append single element to end of list *)
Fixpoint snoc {A : Type} (xs : list A) (x : A) :=
  match xs with
    | [] => [x]
    | (y :: ys) => y :: snoc ys x
  end.

Eval compute in snoc [] 3.
Eval compute in snoc [1; 2] 3.

Lemma snoc_app : forall (ls : list nat) (n : nat),
                   snoc ls n = ls ++ [n].
Proof.
    intros l n.
    induction l.
    (* Case l = [] *)
    reflexivity.
    (* Case l = cons *)
    simpl.
    rewrite IHl.
    reflexivity.
Qed.

(* Interesting way to prove the above using f_equal tactic *)
Lemma snoc_app' : forall (ls : list nat) (n : nat),
                   snoc ls n = ls ++ [n].
Proof.
  induction ls as [| x xs IH]; intro l.
  reflexivity.
  simpl.
  f_equal.
  apply IH.
Qed.

(* Do not quite understand the sequence of tactics here *)
Lemma length_inversion : forall (X : Type) (l : list X),
                           length l = 0 -> l = [].
Proof.
  destruct l; simpl; intro eq.
  reflexivity.
  inversion eq.
Qed.


(* Let this be a good journey with Coq! *)



Fixpoint take {X : Type} (n : nat) (ls : list X) : list X :=
  match (n, ls) with
    | (0, _) => []
    | (S n', []) => []
    | (S n', x :: xs) => x :: take n' xs
  end.
    
Eval compute in take 3 [1; 2; 3; 4; 5].

Fixpoint split {X : Type} (xs : list X) :=
  match xs with
    | (x :: y :: xs') => let (ls, rs) := split xs'
                         in (x :: ls, y :: rs)
    | _ => ([], [])
  end.

Eval compute in split [1; 2; 3; 4].
Eval compute in split [1; 2; 3].

Fixpoint combine {X : Type} (xs : list X) (ys : list X) :=
  match (xs, ys) with
    | (x :: xs', y :: ys') => x :: y :: combine xs' ys'
    | _ => []
  end.

(* Definition uncurry f (x, y) := f x y. *)

Eval compute in combine [1; 2] [3; 4].
Eval compute in let (xs, ys) := split [1; 2; 3; 4]
                in combine xs ys.

(* WellFounded Recursion *)
Section mergeSort.

  Variable A : Type.
  Variable le : A -> A -> bool.

  Fixpoint insert (x : A) (ls : list A) : list A :=
    match ls with
      | nil => x :: nil
      | h :: ls' =>
        if le x h
        then x :: ls
        else h :: insert x ls'
    end.

  Fixpoint merge (ls1 ls2 : list A) : list A :=
    match ls1 with
      | nil => ls2
      | h :: ls1' => insert h (merge ls1' ls2)
    end.

  Fixpoint m_split (ls : list A) : list A * list A :=
    match ls with
      | nil => (nil, nil)
      | h :: nil => (h :: nil, nil)
      | h1 :: h2 :: ls' => let (ls1, ls2) := split ls'
                           in (h1 :: ls1, h2 :: ls2)
    end.

  Print well_founded.

  Print Acc.
                           
  (*
  Fixpoint mergeSort (ls : list A) : list A :=
    if leb (length ls) 1
    then ls
    else let (xs, ys) := split ls
         in merge (mergeSort xs) (mergeSort ys).
   *)

End mergeSort.

Extraction m_split.
Extraction insert.

Theorem n_plus_0 : forall (n : nat), plus n 0 = n.
Proof. 
  intro n.
  induction n.
  (* n = 0 *)
  reflexivity.
  (* n = S n *)
  simpl.
  rewrite IHn.
  reflexivity.
Qed.  

  
(* Proof in a more compact style *)
Theorem n_plus_0' : forall (n : nat), plus n 0 = n.
Proof.
  induction n; simpl; [| rewrite IHn ]; reflexivity.
Qed.

Definition pred' (n : nat) :=
  match n with
    | 0 => 0
    | S n' => n'
  end.

Theorem reduce_me : pred' 1 = 0.
Proof.
  reflexivity.
Qed.

Theorem reduce_me' : pred' 1 = 0.
Proof.
  cbv delta.
  cbv beta.
  cbv iota.
  cbv beta.
  cbv zeta.
  reflexivity.
Qed.

(* Here Coq chooses the first one automatically *)
Fixpoint addLists (ls1 ls2 : list nat) : list nat := 
  match ls1, ls2 with
    | x :: ls1', y :: ls2' => x + y :: addLists ls1' ls2'
    | _, _ => nil
  end.

(* Here we are being explicit on what's the recursive argument is (2nd one) *)
Fixpoint addLists' (ls1 ls2 : list nat) { struct ls2 } : list nat := 
  match ls1, ls2 with
    | x :: ls1', y :: ls2' => x + y :: addLists' ls1' ls2'
    | _, _ => nil
  end.

Theorem nat_t1 : forall (a b : nat),
                   exists (c : nat), a + b = c.
Proof.
  intros.
  eapply ex_intro.
  apply f_equal.
  reflexivity.
Qed.

(*
Proof automation
http://adam.chlipala.net/cpdt/html/Large.html
*)

Inductive exp : Set :=
| Const : nat -> exp
| Plus : exp -> exp -> exp.

Fixpoint eval (e : exp) : nat :=
  match e with
    | Const n => n
    | Plus e1 e2 => (eval e1) + (eval e2)
  end.

Fixpoint times (k : nat) (e : exp) : exp :=
  match e with
    | Const n => Const (k * n)
    | Plus e1 e2 => Plus (times k e1) (times k e2)
  end.

Print mult_plus_distr_l.
Print Nat.mul_add_distr_l.

Theorem eval_times : forall k e,
                       eval (times k e) = k * eval e.
Proof.
  intros.
  induction e.
  trivial.
  simpl.
  rewrite IHe1.
  rewrite IHe2.
  rewrite mult_plus_distr_l.
  reflexivity.
Qed.

Print surjective_pairing.

Lemma surjective_pairing' : forall (A B : Type) (p : A * B), p = pair (fst p) (snd p).
Proof. induction p; reflexivity. Qed.

Lemma inductive_proj : forall (A B : Type) (p1 p2 : A * B),
                         fst p1 = fst p2 -> snd p1 = snd p2 -> p1 = p2.
Abort.

(* Inductive Emp_set : Set :=. *)
(* In standard Coq following does not make sense.  *)
(* Lemma Empty_eq : forall (x : Empty_set) (y : Emp_set), x = y. *)

Print eq.
(* Inductive eq (A : Type) (x : A) : A -> Prop :=  eq_refl : x = x *)

(*
Inductive JMeq (A : Type) (x : A) : forall B : Type, B -> Prop :=
      JMeq_refl : JMeq x x
*)

Goal exists x, 1 + x = 3.
Proof.                          (* |- exists x, 1 + x = 3  *)
  eapply ex_intro.              (* |- 1 + ?x = 3 *)
  simpl.                        (* |- S ?x = 3 *)
  apply f_equal.                (* |- ?x = 2 *)
  reflexivity.                  (* done *)
Qed.

(* Theorem triple_impl : forall P Q : Prop, (((P -> Q) -> Q) -> Q) -> P -> Q. *)
(* Proof. *)
(*   intros H p; apply H; intro H0; apply H0; assumption. *)

Lemma then_example : forall P Q : Prop, (P -> Q) -> P -> Q.
Proof.
  intros p q H; apply H.
Qed.

Lemma triple_impl : forall P Q : Prop, (((P -> Q) -> Q) -> Q) -> P -> Q.
Proof.
  intros P Q H H0; apply H; intros H1; apply H1; assumption.
Qed.

Theorem compose_example : forall P Q R : Prop, (P -> Q -> R) -> (P -> Q) -> (P -> R).
Proof.
  intros P Q R H H' H''.
  apply H. apply H''. apply H'. assumption.
  (* apply H; [assumption | apply H'; assumption]. *)
  (* intros. apply H. apply H1. apply H0. assumption. *)
Qed.

Theorem backward_large : forall A B C : Prop, A -> (A -> B) -> (B -> C) -> C.
Proof.
  intros.
  apply H1.
  apply H0.
  assumption.
Qed.

(* Some interesting examples from - https://coq.inria.fr/tutorial-nahas *)

(* False cannot be proven *)
Goal ~False.
(* One way *)
unfold not.
intro H.
exact H.
(* Another way *)
Restart.
intro H.
case H.

Goal True -> True.
(* a *) intros H; exact H.
Restart.
(* b *) intros H; exact I.

Print I.                        (*  Inductive True : Prop :=  I : True *)

Goal False -> False.
(* a *) intros H. case H.
Restart.
(* b, not recommended *) intros H. exact H.

(* True -> False cannot be proven *)
Goal ~(True -> False).
intros H. case H. exact I.      (* a *)
Restart.
intros H. refine (H _). exact I.

(* Reducto ad absurduium *)
Goal forall A B : Prop, A -> ~A -> B.
intros.
unfold not in H0.
case H0.
exact H.

(* Existence *)
Print ex.

Lemma thm_forall_exists : forall b, (exists a, Is_true (eqb a b)).
Proof.
  intros b.
  case b.
  (* b is true *)
  pose (witness := true).
  Check ex_intro.
  Check (ex_intro _ witness _).
  refine (ex_intro _ witness _).
    simpl; exact I.
  (* b is false *)
  pose (witness := false).
  refine (ex_intro _ witness _).
    simpl; exact I.
Qed.

Theorem forall_exists : forall P : Set -> Prop,
                          forall x, ~(P x) -> ~(exists x, P x).
Proof.
Abort.                          (* Todo *)

Definition hd_never_fail (A : Type) (lst : list A) (prf : lst <> nil) : A :=
  (match lst as b return (lst = b -> A) with
    | nil => (fun xs : lst = nil =>
                match (prf xs) return A with
                end)
    | x :: _ => (fun xs : lst = x :: _ =>
                   x)
  end) eq_refl.

(*
Some proofs from:
http://poleiro.info/posts/2015-04-13-writing-reflective-tactics.html
*)
Definition one_plus_one : 1 + 1 = 2 := eq_refl. (* Coq does simplification by eval so that this works *)

(* Fixpoint n_plus_zero n : n + 0 = n := *)
(*   match n with *)
(*   | O => eq_refl *)
(*   | S x => let erefl := n_plus_zero x in erefl *)
(*   end. *)