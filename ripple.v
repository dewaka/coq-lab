Require Import List.
Require Import Coq.Arith.Plus.

Import ListNotations.

Section MyLists.

  Variable A : Type.

  Definition hd' (default : A) (l : list A) :=
    match l with
      | [] => default
      | x :: _ => x
    end.

  Lemma rev_x : forall (x : A) (l : list A),
                  rev (x :: l) = rev l ++ [ x ].
  Proof.
    auto.
  Qed.

  Lemma null_append_is_id : forall l : list A,
                              l ++ [] = l.
  Proof.
    induction l.
    reflexivity.
    simpl.
    rewrite IHl.
    reflexivity.
  Qed.

  
  Theorem app_assoc : forall l1 l2 l3 : list A,
                        (l1 ++ l2) ++ l3 = l1 ++ (l2 ++ l3).
  Proof.
    intros l1 l2 l3.
    induction l1.
    (* l1 = [] *)
    reflexivity.
    (* l1 = cons x l1' *)
    simpl.
    rewrite IHl1.
    reflexivity.
  Qed.

  Theorem app_length : forall l1 l2 : list A,
                         length (l1 ++ l2) = length l1 + length l2.
  Proof.
    intros l1 l2.
    induction l1.
    (* case l1 = [] *)
    reflexivity.
    (* case l1 = cons x l1' *)
    simpl.
    rewrite IHl1.
    reflexivity.
  Qed.

  Fixpoint snoc (l : list A) (a : A) : list A :=
    match l with
      | [] => [a]
      | h :: t => h :: snoc t a
    end.

  Fixpoint reverse (l : list A) : list A :=
    match l with
      | [] => []
      | h :: t => snoc (reverse t) h
    end.

  Example test1_rev: rev [1; 2; 3] = [3; 2; 1].
  Proof. reflexivity. Qed.

  Theorem length_scoc : forall (x : A) (l : list A),
                          length (snoc l x) = S (length l).
  Proof.
    intros x l.
    induction l.
    (* case l = [] *)
    reflexivity.
    (* case l = cons x l' *)
    simpl.
    rewrite -> IHl.
    reflexivity.
  Qed.

  Theorem rev_length : forall l : list A,
                         length (reverse l) = length l.
  Proof.
    intros l.
    induction l.
    (* case l = [] *)
    reflexivity.
    (* case l = cons x l' *)
    simpl.
    rewrite <- IHl.
    rewrite length_scoc.
    reflexivity.
  Qed.

  (* Exercise: 3 *)
  Lemma app_nil_end : forall l : list A,
                        l ++ [] = l.
  Proof.
    intros l.
    induction l.
    reflexivity.
    simpl.
    rewrite -> IHl.
    reflexivity.
  Qed.

  Lemma silly1 : forall (x y z : nat),
                   y <> 0 ->
                   x + y + y = y + x + y.
  Proof.
    intros.
    rewrite plus_comm.
    rewrite plus_assoc.
    reflexivity.
  Qed.   

  Print silly1.

  

  Lemma silly2 : forall n,
                   0 * n = 0.
  Proof. reflexivity. Qed.

  Print silly2.

  Lemma rev_append_l1 : forall (x : A) (l : list A),
                          rev (x :: l) = rev l ++ [x].
  Proof.
    intros x l.
    induction l.
    (* case l = [] *)
    reflexivity.
    (* case l = cons x l' *)
    simpl.
    (* rewrite <- IHl with . *)
    Abort.

  Lemma rev_involution : forall l : list A,
                           rev (rev l) = l.
  Proof.
    intros l.
    induction l.
    (* case l = [] *)
    reflexivity.
    (* case l = cons x l' *)
    simpl.

    
  Lemma rev_app_distr' : forall x y : list A,
                           rev (x ++ y) = rev y ++ rev x.
  Proof.
    intros x y.
    induction x.
    (* x = [] *)
    induction y.
    (* y = [] *)
    reflexivity.
    (* y = cons x y' *)
    simpl.
    rewrite null_append_is_id.
    reflexivity.
    (* x = cons e x' *)
    induction y.
    (* y = [] *)
    rewrite null_append_is_id in IHx.
    simpl.
    rewrite null_append_is_id.
    reflexivity.
    (* y = cons e y' *)
    simpl.
    rewrite IHx.
    rewrite rev_x.
    
      
  Lemma rev_involutive' : forall l : list A,
                            rev (rev l) = l.
  Proof.
    intro l.
    induction l.
    (* l = [] *)
    reflexivity.
    (* l = cons x l' *)
    rewrite rev_x.
    
  (* Search b_le. *)

  (* Definition range (m n : nat) := *)
  (*   Abort. *)
  (*   if b_leq n m *)
  (*   then n *)
  (*   else m. *)

End MyLists.