Require Import Ring.
Require Import Nat.
Require Import Arith.
Require Import List.

Import ListNotations.

(*
This file tries to prove tail recursive functions using by defining functions
of the same shape as the non-tail version but with extra arguments pushed inside
as placeholders having a similar responsibility as the accumulators in tail
versions.
*)

Section FactFunc.

Fixpoint fact (n : nat) : nat :=
  match n with
    | 0 => 1
    | S n' => n * fact n'
  end.

Fixpoint tfact (n m : nat) : nat :=
  match n with
    | 0 => m
    | S n' => tfact n' (m * n)
  end.

Function cfact (n m : nat) : nat :=
  match n with
    | 0 => m
    | S n' => cfact n' m * n
  end.

Eval compute in tfact 10 0.
Eval compute in cfact 5 1.

Lemma tfact_n_0 : forall n : nat, tfact n 0 = 0.
Proof.
  intro n.
  induction n.
  reflexivity.
  simpl.
  apply IHn.
Qed.

Lemma cfact_intermediate : forall n m : nat,
                             cfact n m + n * cfact n m = cfact n (m * S n).
(*
This is the same as saying:
S n * cfact n m = cfact n (m * S n)
*)
Proof.
  intro n.
  induction n.
  (* case n = 0 *)
  simpl.
  intro m.
  ring.
  (* case n = S n' *)
  intro m.
  Abort.

Lemma cfact_intermediate2 : forall n m : nat, S n * cfact n m = cfact n (m * S n).
Proof.
  intro n.
  induction n.
  (* case n = 0 *)
  simpl.
  intro m.
  ring.
  (* case n = S n' *)
  simpl.
  Abort.


Lemma cfact_plus : forall n a b, cfact n (a + b) = cfact n a + cfact n b.
  induction n.
  intros.
  simpl; ring.
  intros a b.
  unfold cfact.
  fold cfact.
  rewrite (IHn a b).
  ring.
Qed.

Lemma cfact_zero : forall n, cfact n 0 = 0.
  induction n; simpl.
  reflexivity.
  rewrite IHn; simpl; ring.
Qed.

Hint Resolve cfact_zero.

Lemma cfact_interm_kcfactnm : forall k n m, k * cfact n m = cfact n (m * k).
  induction k.
  intros; simpl.
  rewrite (Nat.mul_comm m 0); simpl.
  eauto.
  intros n m.
  simpl.
  rewrite (IHk n m).
  rewrite (Nat.mul_comm m (S k)).
  simpl.
  rewrite (Nat.mul_comm k m).
  rewrite (cfact_plus n m (m * k)).
  reflexivity.
Qed.

Hint Resolve cfact_interm_kcfactnm.

Lemma cfact_interm_cfactnmk : forall n m k, cfact n m * k = cfact n (m * k).
Proof.
  induction k.
  SearchAbout (_ * 0 = _).
  rewrite Nat.mul_0_r.
  assert (m0 : m * 0 = 0).
  ring.
  rewrite m0.
  assert (cf1:  cfact n 0 = 0).
  induction n.
  reflexivity.
  simpl.
  rewrite IHn.
  reflexivity.
  rewrite cf1; reflexivity.
  assert (sk : S k = k + 1).
  ring.
  rewrite sk.
  SearchAbout (_ * (_ + _)).
  rewrite Nat.mul_add_distr_l.
  rewrite IHk.
  rewrite Nat.mul_add_distr_l.
  SearchAbout (_ * 1 = _).
  rewrite Nat.mul_1_r.
  assert (m1 : m * 1 = m).
  ring.
  rewrite m1.
  rewrite cfact_plus.
  reflexivity.
Qed.  

Hint Resolve cfact_interm_cfactnmk.

Lemma cfact_eq_tfact : forall n m : nat, cfact n m = tfact n m.
Proof.
  intro n.
  induction n; [reflexivity | idtac].
  unfold tfact.
  fold tfact.
  intro m.
  unfold cfact.
  fold cfact.
  rewrite <- (IHn (m * (S n))).
  (*
   Here based on the definition of cfact eauto will either use
   cfact_interm_cfactnmk when  S n' => cfact n' m * n
   Or,
   cfact_interm_kcfactnm when S n' => n * cfact n' m
   *)
  eauto.
Qed.

(*
Unsuccessful proof attempts...
I might find something useful below still for use later such as how to use combinations and trys!
*)
  (*   intro n; induction n. *)
  (*   intro; simpl; ring. *)
  (*   intro m. *)
  (*   unfold cfact. *)
  (*   fold cfact. *)
  (*   rewrite (IHn m). *)
  (*     induction n; intros; [ simpl | idtac] ; try ring. *)
  (*     unfold cfact. *)
  (*     fold cfact. *)
  
  (* intros n m. *)
  (* functional induction (cfact n m). *)
  (* (* case n = 0 *) *)
  (* reflexivity. *)
  (* rewrite (IHn0). *)
  (* Check cfact_rect. *)
  (* (* case n = S n' *) *)
  (* intro. *)
  (* simpl. *)

  (* rewrite <- (IHn (m * S n)). *)
  (* (* Gotto prove: cfact n m + n * cfact n m = cfact n (m * S n) *) *)

(* intros. *)
(* induction m. *)
(*   (* case m = 0 *) *)
(*   induction n. *)
(*   reflexivity. *)
(*   simpl. *)
(*   rewrite IHn. *)
(*   simpl. *)
(*   rewrite tfact_n_0; simpl. *)
(*   ring. *)
(*   (* case m = S m' *) *)
(*   induction n. *)
(*   reflexivity. *)
(*   simpl *)

End FactFunc.

Section WSumFunc.

  
Fixpoint wsum (n : nat) : nat :=
  match n with
    | 0 => 0
    | S n' => S n + wsum n'
  end.

Fixpoint wsigma (n y : nat) : nat :=
  match n with
    | 0 => y
    | S n' => wsigma n' (y + (S (S 0)) + n')
  end.

Fixpoint cwsum (n a : nat) : nat :=
  match n with
    | 0 => a
    | S n' => S n + cwsum n' a
  end.

Eval compute in wsum 10.
Eval compute in cwsum 10 0.
Eval compute in wsigma 10 0.

Lemma cwsum_interm_k_plus : forall n a k : nat,
                              cwsum n (a + k) = k + cwsum n a.
Proof.
  intros.
  induction n.
  (* case n = 0 *)
  simpl; ring.
  (* case n = S n' *)
  unfold cwsum.
  fold cwsum.
  rewrite IHn.
  ring.
Qed.

Lemma cwsum_eq_wsigma : forall n a : nat, cwsum n a = wsigma n a.      
Proof.
  intros n.
  induction n.
  (* case n = 0 *)
  reflexivity.
  (* case n = S n' *)
  intro a.
  simpl.
  rewrite <- (IHn (a + 2 + n)).
  Check (cwsum_interm_k_plus n a (2 + n)).
  SearchAbout (_ + (_ + _)).
  pose (H' := cwsum_interm_k_plus n a (2 + n)).
  rewrite Nat.add_assoc in H'.
  rewrite H'.
  ring.
Qed.

Lemma cwsum_n_0_eq_wsum : forall n : nat, cwsum n 0 = wsum n.
Proof.
  intros n.
  induction n.
  reflexivity.
  simpl.
  rewrite IHn.
  ring.
Qed.

Hint Resolve cwsum_eq_wsigma.
Hint Resolve cwsum_n_0_eq_wsum.

(* Thus, ultimately *)
Lemma wsum_eq_wsigma : forall n : nat, wsum n = wsigma n 0.
Proof.
  intros n.
  rewrite <- cwsum_n_0_eq_wsum.
  apply cwsum_eq_wsigma.        (* or eauto *)
Qed.

End WSumFunc.

Section ListStuff.

(* Simple reverse *)
Fixpoint reverse {A} (l : list A) : list A :=
  match l with
    | [] => []
    | x :: xs => reverse xs ++ [x]
  end.

Eval compute in reverse [1; 2; 3].

Fixpoint treverse {A} (l : list A) (acc : list A) : list A :=
  match l with
    | [] => acc
    | x :: xs => treverse xs (x :: acc)
  end.

Eval compute in treverse [1; 2; 3; 4] [].

Fixpoint creverse {A} (l m : list A) : list A :=
  match l with
    | [] => m
    | x :: xs => creverse xs m ++ [x]
  end.

Eval compute in creverse [1; 2; 3; 4] [].


Lemma creverse_interm : forall A (l m : list A) (a : A),
                          creverse l m ++ [a] = creverse l (a :: m).
Proof.
  intros A l.
  induction l.
  (* case l = [] *)
  simpl.
  (* case l = cons _ l' *)
  Abort.

Lemma treverse_interm : forall A (l a b : list A),
                          treverse l a ++ b = treverse l (a ++ b).
Proof.
  intros A l.
  induction l.
  reflexivity.
  simpl.
  intros a0 b.
  rewrite (IHl (a :: a0) b).
  reflexivity.
Qed.

Lemma treverse_interm_1 : forall A (l : list A) (a : A),
                            treverse l [] ++ [a] = treverse l [a].
Proof.
  intros A l.
  intros a.
  pose (tr1 := treverse_interm A l [a] []).
  (* assert (r1 : treverse l ([a] ++ []) = treverse l [a]). *)
  simpl in tr1.
  (* rewrite app_nil_r in tr1. *)
  rewrite <- tr1.
  pose (tr2 := treverse_interm A l [] [a]).
  simpl in tr2.
  rewrite <- tr2.
  rewrite app_nil_r; reflexivity.
Qed.

(* Lemma treverse_interm_2 : forall A (l m : list A), *)
(*                             treverse l [] ++ m = treverse l m. *)
(* Proof. *)
(*   intros A l. *)
(*   induction l. *)
(*   reflexivity. *)
(*   simpl. *)
(*   induction m. *)
(*   SearchAbout (_ ++ []). *)
(*   rewrite app_nil_r; reflexivity. *)
(*   rewrite <- (IHl (a :: a0 :: m)). *)

Lemma creverse_eq_treverse : forall A (l : list A),
                               creverse l [] = treverse l [].
Proof.
  intros A l.
  induction l.
  (* case l = [] *)
  reflexivity.
  (* case l = cons _ l' *)
  simpl.
  rewrite IHl.
  (* Now we got to prove:
     treverse l [] ++ [a] = treverse l [a] *)
  apply treverse_interm_1.
Qed.

Lemma creverse_eq_reverse : forall A (l : list A),
                              creverse l [] = reverse l.
Proof.
  intros A l.
  induction l.
  reflexivity.
  simpl; rewrite IHl; reflexivity.
Qed.

Hint Resolve treverse_interm_1.
Hint Resolve creverse_eq_treverse.
Hint Resolve creverse_eq_reverse.

Lemma reverse_eq_treverse : forall A (l : list A),
                              reverse l = treverse l [].
Proof.
  intros A l.
  induction l.
  reflexivity.
  simpl.
  rewrite <- creverse_eq_reverse.
  eauto.
  rewrite creverse_eq_treverse.
  eauto.
Qed.

(* This is actually wrong! *)
(* Lemma creverse_eq_treverse_general : forall A (l m : list A), *)
(*                                        creverse l m = treverse l m. *)
(* Proof. *)
(*   intros A l. *)
(*   induction l. *)
(*   (* case l = [] *) *)
(*   reflexivity. *)
(*   (* case l = cons _ l' *) *)
(*   intro m. *)
(*   simpl. *)
(*   rewrite <- (IHl (a :: m)). *)
(*   (* Now proof comes down to proving: *)
(*      creverse l m ++ [a] = creverse l (a :: m) *) *)
(*   induction l. *)
(*   simpl. *)

End ListStuff.

Section TreeStuff.

Print list.

Inductive tree (A : Type) : Type :=
| empty : tree A
| node : A -> tree A -> tree A -> tree A.

Arguments empty [A].
Arguments node [A] _ _ _.

Eval compute in (node 3 empty empty).

Fixpoint count {A : Type} (t : tree A) : nat :=
  match t with
    | empty => 0
    | node _ l r => count l + 1 + count r
  end.

Eval compute in count empty.
Eval compute in count (node 3 empty empty).

Fixpoint tcount {A : Type} (t : tree A) (n : nat) : nat :=
  match t with
    | empty => n
    | node _ lt rt => tcount lt (tcount rt (S n))
  end.

Eval compute in tcount empty 0.
Eval compute in tcount (node 3 empty empty) 0.

Fixpoint ccount {A : Type} (t : tree A) (a : nat) : nat :=
  match t with
    | empty => a
    | node _ l r => ccount l a + 1 + ccount r a
  end.

Eval compute in ccount empty 0.
Eval compute in ccount (node 3 empty empty) 0.
Eval compute in ccount (node 3 (node 2 (node 1 empty empty) empty) (node 7 (node 8 empty empty) empty)) 3. (* 23 *)
Eval compute in tcount (node 3 (node 2 (node 1 empty empty) empty) (node 7 (node 8 empty empty) empty)) 3. (* 8 *)
Eval compute in count (node 3 (node 2 (node 1 empty empty) empty) (node 7 (node 8 empty empty) empty)).

(*
Notes
ccount t n = count t is only true when n = 0
ccount t n = tcount t n is only true when n = 0.
This is pretty important as the general case does not hold true.
*)

Lemma tcount_interm_helper : forall (A : Type) (t : tree A) (m k : nat),
    tcount t (m + k) = tcount t (k + m).
Proof.
  intros A t m k.
  rewrite plus_comm; reflexivity.
Qed.

Lemma tcount_interm_m_plus_k : forall (A : Type) (t : tree A) (m k : nat),
    tcount t (m + k) = tcount t m + k.
Proof.
  intros A t.
  induction t.
  reflexivity.

  assert (sub1 : forall m k : nat, tcount t1 (k + m) = tcount t1 m + k).
  intros m k.
  rewrite plus_comm; apply IHt1.

  assert (sub2 : forall m k : nat, tcount t2 (k + m) = tcount t2 m + k).
  intros m k.
  rewrite plus_comm; apply IHt2.

  intros m k.
  simpl.

  rewrite plus_n_Sm.
  SearchAbout plus.
  rewrite plus_comm.
  rewrite plus_Snm_nSm.

  Check (sub2 k (S m)). (* sub2 k (S m) : tcount t2 (S m + k) = tcount t2 k + S m *)
  Check (IHt2 k (S m)). (* IHt2 k (S m) : tcount t2 (k + S m) = tcount t2 k + S m *)
  Check (sub1 k (S m)).

  rewrite (sub2 (S m) k).
  rewrite (IHt1 (tcount t2 (S m)) k).
  reflexivity.
Qed.                            (* Arrrrrrrrrrrrrrrrgh!!! Finally!!!!! *)

Hint Resolve tcount_interm_m_plus_k.

Lemma ccount_eq_count : forall (A : Type) (t : tree A),
    ccount t 0 = count t.
Proof.
  intros A t.
  induction t.
  reflexivity.
  simpl.
  eauto.
Qed.

Hint Resolve ccount_eq_count.

(* Lemma tcount_comm : forall (A : Type) (t1 t2 : tree A) (m : nat), *)
(*     tcount t1 (tcount t2 m) = tcount t2 (tcount t1 m). *)
(* Proof. *)
(*   intros A t1 t2 m. *)
(*   induction t1. *)
(*   reflexivity. *)
(*   simpl. *)
(*   induction t2. *)
(*   reflexivity. *)
(*   simpl. *)

Lemma tcount_interm_2 : forall (A : Type) (t1 t2 : tree A),
    tcount t1 0 + 1 + tcount t2 0 = tcount t1 (tcount t2 1).
Proof.
  intros.

  assert (HSm : forall m : nat, S m = m + 1).
    SearchAbout (_ + 1).
    intro m.
    rewrite <- (Nat.add_1_r m); rewrite plus_comm; reflexivity.

  induction t1.
  induction t2.
  reflexivity.
  simpl.
  rewrite HSm.
  Check (tcount_interm_m_plus_k A t2_1 (tcount t2_2 1) 1).
  rewrite <- (tcount_interm_m_plus_k A t2_1 (tcount t2_2 1) 1).
  rewrite <- (tcount_interm_m_plus_k A t2_2 1 1).
  reflexivity.
  induction t2.
  simpl.
  rewrite <- (tcount_interm_m_plus_k A t1_1 (tcount t1_2 1) 1).
  rewrite <- (tcount_interm_m_plus_k A t1_2 1 1).
  auto.

  simpl.
  rewrite <- (tcount_interm_m_plus_k A t1_1 (tcount t1_2 1) 1).
  rewrite <- (tcount_interm_m_plus_k A t1_1 (tcount t1_2 1 + 1) (tcount t2_1 (tcount t2_2 1))).

  assert (sub1 : 1 + 1 + tcount t2_1 (tcount t2_2 1) = S (tcount t2_1 (tcount t2_2 2))).
    simpl.
    rewrite (HSm (tcount t2_1 (tcount t2_2 1))).
    SearchAbout (1 + 1).
    rewrite BinInt.ZL0.
    rewrite (tcount_interm_m_plus_k A t2_2 1 1).
    rewrite (tcount_interm_m_plus_k A t2_1 (tcount t2_2 1) 1).
    reflexivity.
    
  rewrite <- sub1.
  rewrite <- plus_assoc.
  rewrite <- (tcount_interm_m_plus_k A t1_2 1 (1 + tcount t2_1 (tcount t2_2 1))).
  rewrite plus_assoc.
  reflexivity.
Qed.

Hint Resolve tcount_interm_2.

Lemma ccount_eq_tcount : forall (A : Type) (t : tree A),
    ccount t 0 = tcount t 0.
Proof.
  intros A t.
  induction t.
  reflexivity.
  simpl.
  rewrite IHt1.
  rewrite IHt2.
  eauto.
Qed.



End TreeStuff.

(*
Ok, now trying to follow the same pattern to workout the proof for cfib
definition below which can be used to prove non-tail fib = tail fib
*)
Section FibFunc.

  
Fixpoint fib (n : nat) : nat :=
  match n with
    | 0 => 0
    | S n' => match n' with
                | 0 => 1
                | S n'' => fib n' + fib n''
              end
  end.

Fixpoint tfib (n a b : nat) : nat :=
  match n with
    | 0 => a
    | S n' => tfib n' b (a + b)
  end.

Function cfib (n a b : nat) : nat :=
  match n with
    | 0 => a
    | S n' => match n' with
                | 0 => b
                | S n'' => cfib n' a b + cfib n'' a b
              end
  end.

Eval compute in fib 10.         (* 55 *)
Eval compute in tfib 10 0 1.    (* 55 *)
Eval compute in cfib 10 0 1.    (* 55 *)

(* Trying to define a tfib with the same shape as cfib *)
Fixpoint tfib' (n a b : nat) : nat :=
  match n with
    | 0 => a
    | S n' => match n' with
                | 0 => b
                | S _ => tfib' n' b (a + b)
              end
  end.

Eval compute in tfib' 10 0 1.   (* 55 *)

Lemma cfib_interm_2 : forall n a b : nat,
                        cfib (S (S n)) a b = cfib n (a + b) (b + (a + b)).
Proof.
  intro n.
  induction n.
  (* case n = 0 *)
  intros a b. simpl. ring.
  (* case n = S n' *)
  intros a b.
  assert (lemma1 : forall n a b : nat,
                     cfib (S n) a b = cfib n b (a + b)).
  admit.
  (* Check (lemma1 n (a + b) (b + (a + b))). *)
  (* Check (lemma1 (S n) (a + b) (b + (a + b))). *)
  rewrite (lemma1 n (a + b) (b + (a + b))).
  Abort.

Lemma cfib_interm3 : forall a b n : nat,
                       cfib (S n) a b + cfib n a b = cfib (S n) b (a + b).
Proof.
  intros n.
  induction n.
  simpl.
  induction n.
  simpl; ring.
  simpl.
  rewrite IHn.
  (* At this point it is clear that this can be easily proven if
     cfib (S n) a b = cfib n b (a + b) holds
     which looks like a more fundamental result.

     On the other hand we can prove the above result if this result holds as well.

     How to resolve this situation of mutually dependent properties?
  *)
  Abort.

Lemma tfib_interm : forall n a b : nat,
                      tfib (S n) a b = tfib n b (a + b).
Proof.
  intros n.
  induction n.
  (* case n = 0 *)
  reflexivity.
  (* case n = S n' *)
  
  intros a b.
  rewrite (IHn b (a + b)).
  simpl.
  reflexivity.
Qed.
  
Lemma cfib_interm : forall n a b : nat,
                      cfib (S n) a b = cfib n b (a + b).
Proof.
  intros.
  destruct n.
  reflexivity.
  simpl.
  induction n.
  simpl; ring.
  simpl.
  Abort.


Lemma cfib_interm_1 : forall n a b : nat,
                        cfib (S n) a b = cfib n b (a + b).
Proof.
  induction n.
  (* case n = 0 *)
  reflexivity.
  (* case n = S n' *)
  intros.

  (* From here *)
  (* We can do following destruct; simpl; ring. sequence *)
  (* 
  destruct n.
  simpl; ring.
  destruct n.
  simpl; ring.
  destruct n.
  simpl. ring.
  destruct n. simpl. ring.
  *)
  (* We can go on forever this way, but it is only going to build more an more
  complicated with larger expressions, but not simpler. *)

  (* Or we can use induction hypo for this rewrite when goal is,
     cfib (S (S n)) a b = cfib (S n) b (a + b) 
     to get, 
     cfib (S (S n)) a b = cfib n (a + b) (b + (a + b))
   *)
  rewrite (IHn b (a + b)).
  (* Goal :
     cfib (S (S n)) a b = cfib n (a + b) (b + (a + b))
     cfib (S (S (S n))) a b = cfib n (b + (a + b)) ((a + b) + (b + (a + b)))
     ...
     This pattern is going to continue this way.
   *)
  (* Not getting simpler, so *)
  Restart.                     

  intros n a b.
  functional induction (cfib n a b).
  reflexivity.
  simpl. ring.
  induction n''.
  simpl. ring.
  (* No Progress this way either. *)
  Restart.

  intros n a b.
  functional induction (cfib n b (a + b)).
  (* For: cfib 1 a a0 = a0 *)
  reflexivity.
  (* For: cfib 2 a a0 = b *)
  (* Doesn't make sense *)
  Restart.

  intros n.
  induction n.
  reflexivity.
  intros a b.
  rewrite (IHn b (a + b)).
  Restart.
  
  intros n a b.
  induction n.
  reflexivity.
  Check IHn.
  simpl.
  induction n.
  simpl. ring.
  Abort.
    

Lemma cfib_eq_tfib : forall n a b : nat,
                       cfib n a b = tfib n a b.
Proof.
  induction n.
  (* case n = 0 *)
  reflexivity.
  (* case n = S n *)
  intros.
  unfold tfib.
  fold tfib.
  (* Check IHn. *)
  rewrite <- (IHn b (a + b)).
  (* Intermediate goal: cfib (S n) a b = cfib n b (a + b) *)

  Restart.
  
  (*
  If we can get both sides with tfib then we might be able to use already proven
  Lemma tfib_interm : forall n a b : nat, tfib (S n) a b = tfib n b (a + b).
  *)
  induction n.
  (* case n = 0 *)
  reflexivity.
  (* case n = S n *)
  intros.
  (* unfold cfib. *)
  (* fold cfib. *)
  (* rewrite (IHn a b). *)
  simpl.
  induction n.
  reflexivity.
  simpl.
  Abort.

Lemma cfib_eq_tfib' : forall n a b : nat,
                        cfib n a b = tfib' n a b.
Proof.
  intros n.
  induction n.
  reflexivity.

  simpl.
  intros a b.
  rewrite (IHn a b).

  destruct n.
  reflexivity.
  simpl.
  
  unfold cfib.
  fold cfib.
  Abort.
  
(* Print eq. *)
Print eq.
Print eq_refl.

(* This is a useless proof, but... *)
Lemma fib_interm : forall n : nat,
                      fib (S (S n)) = fib n + fib (S n).
Proof.
  intros n.
  induction n.
  reflexivity.
  rewrite IHn.
  simpl.
  
  


End FibFunc.