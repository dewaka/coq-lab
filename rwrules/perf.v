(* Generating Performance Portable Code Using Rewrite Rules - Steuwer et al. *)

Require Import List.
Import ListNotations.

Set Implicit Arguments.

Inductive vector (X : Type) : nat -> Type :=
| vnil : vector X 0
| vcons {n : nat} (h : X) (v : vector X n) : vector X (S n).

Arguments vnil [X].

(* Local Notation "[]" := (vnil _). *)
(* Local Notation "h :: t" := (vcons _ h t) (at level 60, right associativity). *)

Definition vhd (X : Type) n (v : vector X (S n)) : X :=
  match v with
    | vcons _ x _ => x
  end.

Definition vtl (X : Type) n (v : vector X (S n)) : vector X n :=
  match v with
    | vcons _ _ tl => tl
  end.

(* Examples *)
(* Definition vec1 := vcons 3 (vcons 2 (vcons 1 vnil)). *)
(* Eval compute in vec1. *)
(* Eval compute in vtl vec1. *)
(* Eval compute in vhd vec1. *)

Fixpoint vappend {A} {n} {m} (v1 : vector A n) (v2 : vector A m) : vector A (n+m) :=
  match v1 with
    | vnil => v2
    | vcons _ x tl => vcons x (vappend tl v2)
  end.

Eval compute in vappend (vcons 3 vnil) (vcons 4 vnil).

(*** Section a: Iterate decomposition *****************************************)

(* iterate (I + J) M = iterate I M o iterate J M *)

(*
f function here is A -> A. is this too restrictive?
wouldn't the type be A -> Void i.e. side effect...
in that case we cannot use the following definition
*)
Fixpoint iterate {A : Type} (n : nat) (f : A -> A) (l : list A) :=
  match n with
    | 0 => l
    | S n' => iterate n' f (map f l)
  end.

(* Example case *)
Eval compute in iterate 3 (fun x => x * 2) [1; 2; 3].
Eval compute in iterate 2 (fun x => x * 2) [1; 2; 3].
Eval compute in iterate 5 (fun x => x * 2) [1; 2; 3].
Eval compute in
    let f x := x * 2 in
    let a := iterate 3 f [1; 2; 3]
    in iterate 2 f a.

(* We have to prove that iterate (n + m) f l = iterate n f (iterate m f l) *)

Lemma iterate_0_id : forall (A : Type) (f : A -> A) (l : list A),
                       iterate 0 f l = l.
Proof.
  intros.
  induction l. simpl. reflexivity.
  simpl.
  reflexivity.
Qed.

Lemma iterate_nil : forall (A : Type) (n : nat) (f : A -> A),
                      iterate n f [] = [].
Proof.
  intros.
  induction n.
  simpl.
  reflexivity.
  simpl.
  rewrite IHn.
  reflexivity.
Qed.

(* Do we need this? *)
Lemma map_nil : forall (A B : Type) (f : A -> B),
                  map f [] = [].
Proof.
  auto.
Qed.

Lemma iterate_distributive : forall (A : Type) (n m : nat) (f : A -> A) (l : list A),
                               iterate (n + m) f l = iterate n f (iterate m f l).
Proof.
  (* inductoin on list *)
  induction l.
  (* l = nil case *)
  (* induction on n now *)
  induction n.
  reflexivity.                  (* case n=0 *)
  simpl.                        (* case n *)
  Check iterate_nil.
  (* rewrite <- iterate_nil. *)
Abort.
  
(*** Section b: Reorder commutativity *****************************************)

(*
What is reordering?
Most generally a random permutation?
TODO: improve the following reorder operation which might change proofs.
*)

Fixpoint reverse {A : Type} (l : list A) :=
  match l with
    | nil => nil
    | x :: xs => reverse xs ++ [x]
  end.

(* For now we'll define reorder as reversing *)
Definition reorder {A : Type} (l : list A) := reverse l.

Lemma map_append_singleton : forall (A B : Type) (f : A -> B) (x : A) (xs : list A),
                               map f (xs ++ [x]) = map f xs ++ [f x].
Proof.
  intros.
  induction xs.
  reflexivity.
  simpl.
  rewrite IHxs.
  reflexivity.
Qed.

(* map M o reorder = reorder o map M *)
Lemma map_reorder_is_reorder_map : forall (A B : Type) (f : A -> B) (l : list A),
                                     map f (reorder l) = reorder (map f l).
Proof.                                     
  intros.
  induction l.
  reflexivity.
  simpl.
  rewrite <- IHl.
  simpl.
  apply map_append_singleton.
Qed.  

(* reorder o map M = map M o reorder *)
Lemma reorder_map_is_map_reorder : forall (A B : Type) (f : A -> B) (l : list A),
                                     reorder (map f l) = map f (reorder l).
Proof.
  (* There must be a simpler way to use the above proof to do this one *)
  intros.
  induction l.
  reflexivity.
  simpl.
  rewrite IHl.
  rewrite map_append_singleton.
  reflexivity.
Qed.

(*** Section d: Cancellation rules ********************************************)

Fixpoint foldl (A B : Type) (a : B) (f : B -> A -> B) (l : list A) : B :=
  match l with
    | nil => a
    | x :: xs => foldl (f a x) f xs
  end.

Eval compute in foldl 1 (fun x y => x * y) [1; 2; 3; 4; 5]. (* 120, alright... *)

(* join o split I = id *)

(* join [[]] -> [] *)

Definition join {A : Type} (ls : list (list A)) : list A :=
  foldl [] (fun x y => x ++ y) ls.

Eval compute in join [[1; 2]; [3; 4]].

(* We are going to define join as a foldl with a particular operation *)

(*
Split operation
We are going to define split operation as mapping some work on chunks of a list.
 *)

Fixpoint take {A : Type} (n : nat) (l : list A) : list A :=
  match n, l with
    | 0, _ => nil
    | S n', nil => nil
    | S n', x :: xs => x :: take n' xs
  end.

Fixpoint drop {A : Type} (n : nat) (l : list A) : list A :=
  match n, l with
    | 0, _ => l
    | S n', nil => nil
    | S n', _ :: xs => drop n' xs
  end.

Fixpoint take_and_rest {A : Type} (n : nat) (l : list A) {struct l} :=
  match n, l with
    | 0, _ => (nil, l)
    | S n', nil => (nil, nil)
    | S 0, x :: xs => ([x], xs)
    | S n', x :: xs => let (ts, rs) := take_and_rest n' xs in (x :: ts, rs)
  end.

Eval compute in take 3 [1; 2; 3; 4].
Eval compute in drop 3 [1; 2; 3; 4].
Eval compute in take_and_rest 3 [1; 2; 3; 4].

Fixpoint chunk'
         {A : Type}
         (n : nat)
         (xs : list A)
         (k : nat -> list A -> list (list A) -> list (list A)) : list (list A) :=
  match xs with
    | nil => k n nil nil
    | x :: xs' => chunk' n xs'
                         (fun sz rs acc =>
                            match sz with
                              | 0 => k (pred n) [x] (rs :: acc)
                              | S sz' => k sz' (x :: rs) acc
                            end)
  end.

Definition chunk {A : Type} (n : nat) (ls : list A) : list (list A) :=
  chunk' n ls (fun _ xs xss => xs :: xss).

Definition split {A : Type} (n : nat) (l : list A) := chunk n l.

Eval compute in split 2 [1; 2; 3; 4; 5; 6].
Eval compute in split 2 [1; 2; 3; 4; 5; 6; 7].

(*** Section f: Fusion rules **************************************************)
(* map M o map N = map (M o N) *)
Lemma map_fusion : forall (A B C : Type) (f : A -> B) (g : B -> C) (l : list A),
                   map g (map f l) = map (fun x : A => g (f x)) l.
Proof.
  induction l; simpl; auto.
  rewrite IHl; auto.
Qed.

