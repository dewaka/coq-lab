Require Import List.
Require Import Arith.
Require Import Omega.
Require Import Even.

Import ListNotations.

Definition my_map {A B : Type} (f : A -> B) (xs : list A) :=
  match xs with
    | nil => nil
    | (x :: xs') => f x :: map f xs'
  end.

Eval compute in map (fun x => x + 3) (1 :: 2 :: 3 :: nil).
Eval compute in my_map (fun x => x * 3) (1 :: 2 :: 3 :: nil).

SearchAbout map.

Lemma map_map' : forall (A B C : Type) (f : A -> B) (g : B -> C) (l : list A),
                   map g (map f l) = map (fun x : A => g (f x)) l.
Proof.
  induction l; simpl; auto.
  rewrite IHl; auto.
Qed.

Fixpoint insert n l :=
  match l with
    | nil => n :: nil
    | x :: tl => if leb n x then n :: l else x :: insert n tl
  end.

Fixpoint sort l :=
  match l with
    | nil => nil
    | x :: tl => insert x (sort tl)
  end.
       
Eval compute in sort (1 :: 3 :: 2 :: nil).

Search le.

SearchPattern (_ + _ <= _ + _).

SearchRewrite (_ + (_ - _)).

Lemma ex1 : forall a b : Prop,
              a /\ b -> b /\ a.
Proof.
  intros a b H.
  split.
  destruct H as [H1 H2].
  exact H2.
  intuition.
Qed.

(* Lemma ex2 : forall a b : Prop, *)
(*               a /\ b -> b /\ a. *)
(* Proof. *)
(*   intros a b H. *)
(*   split. *)
(*   destruct H as [H1 H2]. *)
(*   auto. *)
(*   intuition. *)
(* Qed. *)

Lemma ex3 : forall A B, A \/ B -> B \/ A.
Proof.
  intros a b H.
  destruct H as [H1 | H2].
  right; assumption.
  left; assumption.
Qed.

(* Rewrite example *)
(* Lemma ex6 : forall x y, *)
(*               (x + y) * (x + y) = x*x + 2*x*y + y*y. *)
(* Proof. *)
(*   intros x y. *)
(*   rewrite mult_plus_distr_r. *)
(*   rewrite mult_plus_distr_r. *)
(*   rewrite plus_assoc. *)
(*   rewrite <- plus_assoc with (n := x * x). *)
(*   rewrite mult_comm with (n := y) (m := x). *)
(*   pattern (x * y) at 1; rewrite <- mult_1_l. *)
(*   rewrite <- mult_succ_l. *)
(*   rewrite <- mult_assoc. *)
(*   reflexivity. *)
(* Qed. *)

Lemma omega_example: forall f x y,
                       0 < x -> 0 < f x -> 3 * f x <= 2 * y -> f x <= y.
Proof.
  intros.
  omega.
Qed.

Print omega_example.           (* This one is not so simple to read :) *)

Fixpoint sum_n (n : nat) :=
  match n with
    | 0 => 0
    | (S n') => n + sum_n n'
  end.
      
Lemma sum_n_p : forall n, 2 * sum_n n + n = n * n.
Proof.
  induction n.
  (* 0 case *)
  reflexivity.
  (* n case *)
  assert (SnSn : S n * S n = n * n + 2 * n + 1).
  ring.
  rewrite SnSn.
  rewrite <- IHn.
  simpl.
Abort.

(* There's a problem with the proof given in the book - Coq in a Hurry *)

Fixpoint evenb (n : nat) : bool :=
  match n with
    | 0 => true
    | S 0 => false
    | S (S n') => evenb n'
  end.

Eval compute in evenb 2000.
Eval compute in evenb 3.

Lemma evenb_p : forall n, evenb n = true -> exists x, n = 2 * x.
Proof.
  assert (Main: forall n, (evenb n = true -> exists x, n = 2 * x) /\
                          (evenb (S n) = true -> exists x, S n = 2 * x)).
  
  induction n.
  split.
  exists 0; ring.
  simpl; intros H; discriminate H.

  split.
  destruct IHn as [_ IHn']; exact IHn'.
  simpl; intros H; destruct IHn as [IHn' _].
  assert (H' : exists x, n = 2 * x).
  apply IHn'; exact H.
  destruct H' as [x q]; exists (x + 1); rewrite q; ring.
Abort.

Fixpoint count n l :=
  match l with
    | nil => 0
    | a :: tl => let r := count n tl
                 in if beq_nat n a then 1+r else r
  end.

Lemma insert_incr : forall n l,
                      count n (insert n l) = 1 + count n l.
Proof.
  intros n l.
  induction l.
  simpl.
  rewrite <- beq_nat_refl.
  reflexivity.
  simpl.
  case (leb n a).
  simpl.
  rewrite <- beq_nat_refl.
  reflexivity.
  simpl.
  case (beq_nat n a).
  rewrite IHl; reflexivity.
  rewrite IHl; reflexivity.
Qed.

(* fold/unfold tactic example *)
Theorem cons_injective :
  forall (A : Set)(a b : A)(l m : list A),
    a :: l = b :: m -> l = m.
Proof.
  intros.
  fold (tail (cons a l)).
  rewrite H.
  unfold tail.
  reflexivity.
Qed.

Theorem plus_assoc' : forall n m p : nat,
                        n + (m + p) = (n + m) + p.
Proof.
  intros.
  induction n.
  reflexivity.
  simpl.
  (* rewrite IHn. *)
  (* reflexivity. *)
  (* Above is another way to prove *)
  Check f_equal.
  apply f_equal.
  apply IHn.
Qed.

(* Case of fix not finding decreasing arguments automatically *)

Inductive Tree :=
| Tip : Tree
| Bin : Tree -> Tree -> Tree.

Inductive TreePair :=
| TipTip : TreePair
| TipBin : Tree -> Tree -> TreePair
| BinTip : Tree -> Tree -> TreePair
| BinBin : TreePair -> TreePair -> TreePair.

(* This definition does not work well with fix *)
(* Fixpoint pair' (l r : Tree) : TreePair := *)
(*   match l with *)
(*     | Tip => match r with *)
(*                | Tip => TipTip *)
(*                | Bin rl rr => TipBin rl rr *)
(*              end *)
(*     | Bin ll lr => match r with *)
(*                      | Tip => BinTip ll lr *)
(*                      | Bin rl rr => BinBin (pair' l rl) (pair' lr r) *)
(*                    end *)
(*   end. *)
      
(*
On the other hand, following is fine.
Using the fix in fix pattern when at least one argument is becoming smaller.
*)

Fixpoint pair' l := fix pair1 (r : Tree) :=
  match l with
    | Tip => match r with
               | Tip => TipTip
               | Bin rl rr => TipBin rl rr
             end
    | Bin ll lr => match r with
                     | Tip => BinTip ll lr
                     | Bin rl rr => BinBin (pair1 rl) (pair' lr r)
                   end
  end.

Eval compute in pair' Tip Tip.
Eval compute in pair' (Bin Tip (Bin Tip Tip)) (Bin Tip Tip).

(*
Ok...
Now the question is how to follow the same structure with chunking
*)

Fixpoint take {A : Type} (n : nat) (l : list A) : list A :=
  match n, l with
    | 0, _ => nil
    | S n', nil => nil
    | S n', x :: xs => x :: take n' xs
  end.

Fixpoint drop {A : Type} (n : nat) (l : list A) : list A :=
  match n, l with
    | 0, _ => l
    | S n', nil => nil
    | S n', _ :: xs => drop n' xs
  end.

(*
Fixpoint chunk {A : Type} (n : nat) (l : list A) (ll : list (list A)) : list (list A) :=
  match l with
    | nil => ll
    | _ => let ts := take n l in
           let rs := drop n l in
           chunk n rs (ts :: ll)
  end.
*)

Fixpoint flat_map {X Y : Type} (f : X -> list Y) (l : list X) : list Y :=
  match l with
    | nil => nil
    | x :: xs => f x ++ flat_map f xs
  end.
      
Eval compute in flat_map (fun n => [n; n; n]) [1; 5; 4].

Definition plus3 x := 3 + x.

Lemma unfold_example : forall m n,
                         3 + n = m -> plus3 n + 1 = m + 1.
Proof.
  intros m n H.
  unfold plus3.
  SearchAbout (_ + _).
  rewrite H.
  reflexivity.
Qed.

Definition override {X : Type} (f : nat -> X) (k : nat) (x : X) : nat -> X :=
  fun (k' : nat) => if beq_nat k k' then x else f k'.

Lemma override_eq : forall { X : Type } x k (f : nat -> X),
                      (override f k x) k = x.
Proof.
  intros.
  unfold override.
  SearchAbout beq_nat.
  rewrite <- beq_nat_refl.
  reflexivity.
Qed.

Lemma override_neq : forall (X : Type) x1 x2 k1 k2 (f : nat -> X),
                       f k1 = x1 ->
                       beq_nat k2 k1 = false ->
                       (override f k2 x2) k1 = x1.
Proof.
  intros.
  unfold override.
  rewrite H0.
  rewrite H.
  reflexivity.
Qed.

Fixpoint take' {A : Type} (n : nat) (ls : list A) : list A :=
  match n, ls with
    | 0, _ => nil
    | _, nil => nil
    | S n', x :: ls' => x :: take' n' ls'
  end.

Fixpoint leb' (n m : nat) : bool :=
  match n, m with
    | 0, _ => true
    | S n', 0 => false
    | S n', S m' => leb' n' m'
  end.

Eval compute in leb' 3 4.
Eval compute in leb' 4 4.
Eval compute in leb' 4 1.

(* ack function
A(m, n) = n + 1 if m = 0
        | A(m - 1, 1) if m > 0 and n = 0
        | A(m - 1, A(m, n-1)) if m > 0 and n > 0
*)
(* This definition does not work becuase Coq cannot guess decreasing arg for fix *)
(*
Fixpoint ack (m n : nat) : nat :=
  match m with
    | 0 => n + 1
    | S m' => match n with
                | 0 => ack m' 1
                | S n' => ack m' (ack m n')
              end
  end.
*)

(* Here's the fixed version *)
Fixpoint ack (m n : nat) : nat :=
  match m with
    | 0 => S n
    | S m' => let fix ackn (n : nat) {struct n} :=
                  match n with
                    | 0 => ack m' 1
                    | S n' => ack m' (ackn n')
                  end
              in ackn n
  end.

Eval compute in ack 3 4.

Check take.
Check drop.

Eval compute in 3 = 3.
Eval compute in 3 > 3.

Eval compute in le 3 4.

Fixpoint lb (m n : nat) : bool :=
  match m, n with
    | 0, S _ => true
    | _, 0 => false
    | S m', S n' => lb m' n'
  end.

Fixpoint eqb (m n : nat) : bool :=
  match m, n with
    | 0, 0 => true
    | S m', S n' => eqb m' n'
    | _, _ => false
  end.

Eval compute in eqb 3 4.
Eval compute in lb 3 4.
Eval compute in lb 4 4.

Fixpoint plus' (m n : nat)  {struct m} : nat :=
  match m with
    | 0 => n
    | S m' => S (plus' m' n)
  end.
(* match m with 0 => n | S m' => S (plus' m' n) *)

Eval compute in plus' 3 4.

Fixpoint iterate {A : Type} (f : A -> A) (n : nat) (x : A) { struct n } : A :=
  match n with
    | 0 => x
    | S n' => iterate f n' (f x)
  end.

Eval compute in iterate S 3 1.

(* We can define ackerman function using iterate *)
Definition ack' (n : nat) : nat -> nat :=
  iterate (fun (f : nat -> nat) (p : nat) =>
             iterate f (S p) 1)
          n
          S.

(* Defining plus with iterate *)
Definition plus'' : nat -> nat -> nat :=
  iterate S.

Definition mult'' (n p : nat) : nat :=
  iterate (plus'' n) p 0.

Eval compute in plus'' 3 4.
Eval compute in mult'' 3 4.

Eval compute in ack' 1 1.
Eval compute in ack 1 1.

(* We can extract functions even to Haskell code as follows *)
(* Default language is OCaml *)
Extraction Language Haskell.

Extraction ack'.
Extraction ack.
Extraction iterate.

Extraction Language Scheme.
Extraction iterate.             (* Pretty with Scheme support as well *)

Print Z.

Fixpoint fib (n : nat) : nat :=
  match n with
    | 0 => 0
    | S p => match p with 0 => 1 | S p' => fib p + fib p' end
  end.

Eval compute in fib 5.
Eval compute in fib 10.

Theorem plus_assoc'' : forall (a b c : nat),
                        a + b + c = a + (b + c).
Proof.
  intros a b c.
  elim a.
  simpl.
  reflexivity.
  simpl.
  intros n H.
  rewrite H.
  reflexivity.
Qed.

Fixpoint split' {A B : Type} (ls : list (prod A B)) : prod (list A) (list B) :=
  match ls with
    | [] => pair nil nil
    | (pair x y) :: rs => match split' rs with
                              pair xs ys => pair (x :: xs) (y :: ys)
                          end
  end.

Example simple_pat1 : 3*3 + 3*3 = 18.
reflexivity.
Qed.

(* Let's think about merge function again *)

Section Zipper.
  Variable A B C : Type.
  Variable f : A -> B -> C.
  
  Fixpoint zip (xs : list A) (ys : list B) : list (A * B) :=
    match xs, ys with
      | nil, _ => nil
      | _, nil => nil
      | x :: xs', y :: ys' => (x, y) :: zip xs' ys'
    end.

  Fixpoint zipWith (xs : list A) (ys : list B) : list C :=
    match xs, ys with
      | nil, _ => nil
      | _, nil => nil
      | x :: xs', y :: ys' => (f x y) :: zipWith xs' ys'
    end.

End Zipper.

Implicit Arguments zip [A B].
Implicit Arguments zipWith [A B C].

Eval compute in zipWith (fun x y => 2 * x + y) [1; 2; 3] [4; 5].
Eval compute in zip [1; 2; 3] [4; 5].
Eval compute in zipWith (fun x y => (x, y)) [1; 2; 3] [4; 5]. 

Section Merge.
  Variable A : Type.
  Variable less_or_eq : A -> A -> bool.

  (* This definition would not pass fix requirements *)
  (*
  Fixpoint merge (xs : list A) (ys : list A) : list A :=
    match xs, ys with
      | nil, _ => ys
      | _, nil => xs
      | x :: xs', y :: ys' => if less_or_eq x y
                              then x :: merge xs' ys
                              else y :: merge xs ys'
    end.
  *)

  Fixpoint merge (xs : list A) : list A -> list A :=
    match xs with
      | nil => fun ys => ys
      | x :: xs' => let fix merge_aux ys :=
                        match ys with
                          | nil => xs
                          | y :: ys' => if less_or_eq x y
                                        then x :: merge xs' ys
                                        else y :: merge_aux ys'
                        end
                    in merge_aux 
    end.

End Merge.

Implicit Arguments merge [A].

Eval compute in merge leb [1; 2; 3] [4; 5; 6].
Eval compute in merge leb [4; 5; 6] [1; 2; 3].

Fixpoint foldl {A B : Type} (a : B) (f : B -> A -> B) (l : list A) : B :=
  match l with
    | nil => a
    | x :: xs => foldl (f a x) f xs
  end.

Function fact (n : nat) : nat :=
  match n with
    | 0 => 1
    | S n' => n * fact n'
  end.

Function take_fn {A : Type} (n : nat) (ls : list A) : list A :=
  match n, ls with
    | 0, _ => nil
    | _, nil => nil
    | S n', h :: t => h :: take_fn n' t
  end.

Function drop_fn {A : Type} (n : nat) (ls : list A) : list A :=
  match n, ls with
    | S n', _ :: t => drop_fn n' t
    | _, _ => ls
  end.

Eval compute in drop_fn 2 [1; 2; 3].
Eval compute in take_fn 2 [1; 2; 3].

(*
Section Chunk.
  Variable A : Type.

  Fixpoint chunk {A : Type} (n : nat) (xs : list A) (acc : list (list A)) : list (list A) :=
    match xs with
      | nil => acc
      | x :: xs' => let fix chunk' (sz : nat) (rs : list A) : list (list A) :=
                        match sz with
                          | 0 => chunk n xs' (rs :: acc)
                          | S sz' => chunk' sz' (x :: rs)
                        end
                    in chunk' n []
    end.
        

End Chunk.

Implicit Arguments chunk [A].

Eval compute in chunk 2 [1; 2; 3; 4; 5] [].
*)

(* Fixpoint chunk {A : Type} (n : nat) (xs : list A) (acc : list (list A)) : list (list A) := *)
(*   acc. *)
  
(* Fixpoint chunk {A : Type} (n : nat) (xs : list A) {struct xs} : list (list A) := *)
(*   match xs with *)
(*     | nil => nil *)
(*     | h :: t => let fix chunk' (size : nat) (acc : list A) (rs : list A) {struct size} : list (list A) := *)
(*                     match size with *)
(*                       | 0 => acc :: chunk n t *)
(*                       | S n' => chunk' n' (h :: acc) t *)
(*                     end *)
(*                 in chunk' n [] xs *)
(*   end. *)

(* Eval compute in chunk 2 [1; 2; 3; 4]. *)

(* Function chunk {A : Type} (n : nat) (ls : list A) {struct ls} : list (list A) := *)
(*   match ls with *)
(*     | nil => [] *)

(*     | _ => take_fn n ls :: chunk n (drop_fn n ls) (* Problem place... still *) *)
(*   end. *)

(* Factorial in continuation passing style *)
Fixpoint fact_k n k : nat :=
  match n with
    | 0 => k 1
    | S n' => fact_k n' (fun x => k (n * x))
  end.

Eval compute in fact_k 5 (fun x => x).

Fixpoint fib_k n k : nat :=
  match n with
    | 0 => k 0
    | S n' => match n' with
                | 0 => k 1
                | S n'' => fib_k n' (fun v1 => fib_k n'' (fun v2 => k (v1 + v2)))
              end
  end.

Eval compute in fib_k 10 (fun x => x).

Fixpoint chunk'
  {A : Set}
  (sz : nat)
  (xs : list A)
  (k : nat -> list A -> list (list A) -> list (list A)) : list (list A) :=
  match xs with
    | nil => k sz nil nil
    | x :: xs' => chunk' sz xs'
                         (fun n rs acc =>
                            match n with
                              | 0 => k (pred sz) (x :: nil) (rs :: acc)
                              | S n' => k n' (x :: rs) acc
                            end)
  end.

Definition chunk {A : Set} (n : nat) (xs : list A) : list (list A) :=
  chunk' n xs (fun _ rs acc => rs :: acc).

Eval compute in chunk 2 [1; 2; 3; 4; 5; 6].