Require Import Recdef.
Require Import Wf.
Require Import List.
Require Import String ZArith.

Open Scope Z_scope.
Import ListNotations.

Inductive aexpr : Type :=
| avar (x : string)
| anum (n : Z)
| aplus (e1 e2 : aexpr).

Fixpoint ev (g : string -> Z) (e : aexpr) : Z :=
  match e with
    | avar x => g x
    | anum n => n
    | aplus e1 e2 => ev g e1 + ev g e2
  end.

(* Fixpoint fact x := *)
(*   match x with | 0 => 1 | S p => x * fact p end. *)

Close Scope Z_scope.

Eval compute in fact 5.

Fixpoint div2 x :=
  match x with
    | S (S p) => S (div2 p)
    | _ => 0
  end.

Functional Scheme div2_ind := Induction for div2 Sort Prop.

Lemma div2t2le : forall x, div2 x * 2 <= x.
intros x; functional induction div2 x.
omega.
omega.
omega.
Qed.

Eval compute in pred 3.

Fixpoint sd (n : nat) :=
  match n with
    | 0 => 0
    | S p => S (sd (pred p))
  end.

Eval compute in sd 3.
Eval compute in sd 6.
Eval compute in sd 14.
Eval compute in sd 25.
Eval compute in sd 100.

(* Program Fixpoint div2 (n : nat) {measure n} : *)
(*   { x : nat | n = 2 * x \/ n = 2 * x + 1 } := *)
(*   match n with *)
(*     | S (S p) => S (div2 p) *)
(*     | _ => 0 *)
(*   end. *)

(*
Check Zabs.
Eval compute in Zabs (-3).

Function fact (x : Z) {measure Zabs x} :=
  if Zle_bool x 0
  then 1
  else x * fact (x - 1).
*)

Fixpoint chunk'
         {A : Type}
         (n : nat)
         (xs : list A)
         (k : nat -> list A -> list (list A) -> list (list A)) : list (list A) :=
  match xs with
    | nil => k n nil nil
    | x :: xs' => chunk' n xs'
                         (fun sz cs xss =>
                            match sz with
                              | 0 => k (pred n) [x] (cs :: xss)
                              | S sz' => k sz' (x :: cs) xss
                            end)
  end.
      
Definition chunk {A : Type} (n : nat) (xs : list A) : list (list A) :=
  chunk' n xs (fun _ xs acc => xs :: acc).

Eval compute in chunk 2 [1; 2; 3; 4; 5].

Program Definition lhead {A} (xs : {l:list A | l <> []}) : A :=
  match xs with [] => _ | x::xs => x end.

Eval compute in lhead [23; 43].