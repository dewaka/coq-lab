(* http://adam.chlipala.net/cpdt/html/Equality.html *)
Definition pred' (x : nat) :=
  match x with
    | 0 => x
    | S x' => let y := x' in y
  end.

Theorem red : pred' 1 = 0.
Proof. reflexivity. Qed.

Print red.

Theorem red' : pred' 1 = 0.
Proof.
  cbv delta.                    (* To unfold global definitions *)
  cbv beta.                     (* standard beta red *)
  cbv iota.                     (* simplify a single match term *)
  cbv beta.
  cbv zeta.                     (* replaces let body with appropriate substitutions *)
  reflexivity.
Qed.