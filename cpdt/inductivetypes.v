Add LoadPath "~/Dropbox/Projects/Coq/cpdtlib/".

Require Import List.

Set Implicit Arguments.
(* Set Asymmetric Patterns. *)
 
Require Import CpdtTactics.

Check (fun x : nat => x).
Check (fun x : True => x).

Check I.

Check (fun _ : False => I).
Check (fun x : False => x).

Check tt.
Check unit.
Check Set.
Check Type.

Theorem unit_singleton : forall x : unit, x = tt.
Proof.
  induction x.
  reflexivity.
Qed.

Check unit_ind.

Check Empty_set.

(* Empty_set is defined as follows *)
(* Inductive Empty_set : Set :=.  *)

Theorem the_sky_is_falling : forall x : Empty_set, 2 + 2 = 5.
Proof.
  destruct 1.
Qed.

Check Empty_set_ind.

(*
This is a curious case. Since Empty_set does not have any constructors
we do not have to provide anything for match eitehr
*)
Definition e2u (e : Empty_set) : unit := match e with end.

Inductive coin_toss : Set :=
| head
| tail.

Definition flip_toss (t : coin_toss) : coin_toss :=
  if t (* Here t is checked for eqivalance to head, the first element of the inductive type *)
  then tail
  else head.

Eval compute in (flip_toss tail).
Eval compute in (flip_toss head).

Theorem flip_toss_inverse : forall t : coin_toss,
                              flip_toss (flip_toss t) = t.
Proof.
  destruct t.
  reflexivity.
  Restart.                      (* Restarting to give a more compact proof *)
  destruct t; reflexivity.
Qed.

Theorem flip_toss_ineq : forall t : coin_toss,
                           flip_toss t <> t.
Proof.
  destruct t; discriminate.
Qed.

Theorem zero_plus_n : forall n : nat, plus 0 n = n.
Proof.
  intro; reflexivity.
Qed.

Theorem n_plus_zero : forall n : nat, plus n 0 = n.
Proof.
  intro n.
  induction n.
  reflexivity.
  simpl.
  rewrite IHn.
  reflexivity.
Qed.

(* Now we can do the same much more easily with Cpdt crush tactic *)
Theorem n_plus_zero' : forall n : nat, plus n 0 = n.
Proof.
  induction n; crush.
Qed.

Print n_plus_zero'.

Check nat_ind.

Theorem S_inj : forall n m : nat, S n = S m -> n = m.
Proof.
  (* auto. *)
  injection 1; trivial.
Qed.

Inductive btree (T : Type) : Type :=
| Leaf : btree T
| Node : btree T -> T -> btree T -> btree T.

Implicit Arguments Leaf [T].
Implicit Arguments Node [T].

Fixpoint nsize {A : Type} (tr : btree A) : nat :=
  match tr with
    | Leaf => 0
    | Node l _ r => nsize l + 1 + nsize r
  end.
                
Eval compute in nsize (Node Leaf 1 Leaf).
Eval compute in nsize (Leaf).

Fixpoint splice {A : Type} (tr1 tr2 : btree A) : btree A :=
  match tr1 with
    | Leaf => tr2
    | Node l x r => Node (splice l tr2) x r
  end.

Definition t1 := Node Leaf 1 Leaf.
Definition t2 := Node (Node Leaf 4 Leaf) 3 Leaf.

Eval compute in splice t2 t1.

Theorem plus_assoc : forall a b c : nat,
                       plus (plus a b) c = plus a (plus b c).
Proof.
  intros a b c.
  induction a.
  reflexivity.
  simpl.
  rewrite IHa.
  reflexivity.
  (* Alternatively *)
  Restart.
  induction a; crush.           (* This is pretty simple *)
Qed.

Theorem size_splice : forall (A : Type) (t1 t2 : btree A),
  nsize (splice t1 t2) = plus (nsize t1) (nsize t2).
Proof.
  intros A t1 t2.
  induction t1.
  reflexivity.
  simpl.
  (* next step involves a rewriting a lot with assoc rules *)
  (* so, i'm automating that with crush *)
  crush.
  (* Alternative shorter proof *)
  Restart.
  intros A t1 t2.
  Hint Rewrite n_plus_zero plus_assoc.
  induction t1; crush.
Qed.

Check btree_ind.

Theorem length_app : forall T (ls1 ls2 : list T),
                       length (app ls1 ls2) = plus (length ls1) (length ls2).
Proof.
  induction ls1; crush.
Qed.

Section mlist.
  Variable T : Type.

  Inductive mlist : Type :=
  | MNil : mlist
  | MCons : T -> mlist -> mlist.

  Fixpoint mlength (ls : mlist) : nat :=
    match ls with
      | MNil => 0
      | MCons _ ls' => S (mlength ls')
    end.

  Fixpoint mapp (xs ys : mlist) : mlist :=
    match xs with
      | MNil => ys
      | MCons x xs' => MCons x (mapp xs' ys)
    end.

End mlist.

Implicit Arguments MNil [T].

Check MNil.
Check MCons.

Print mlist.

Check mlist_ind.

(* Mutually Inductive Types *)

Inductive even_list : Type :=
| ENil : even_list
| ECons : nat -> odd_list -> even_list
with odd_list : Type :=
| OCons : nat -> even_list -> odd_list.
   
Fixpoint elength (el : even_list) : nat :=
  match el with
    | ENil => 0
    | ECons _ ol => 1 + olength ol
  end
with olength (ol : odd_list) : nat :=
  match ol with
    | OCons _ el => 1 + elength el
  end.

Eval compute in elength ENil.
Eval compute in elength (ECons 1 (OCons 2 ENil)).
Eval compute in olength (OCons 3 (ECons 1 (OCons 2 ENil))).
      
Fixpoint eapp (el1 el2 : even_list) : even_list :=
  match el1 with
    | ENil => el2
    | ECons x ol => ECons x (oapp ol el2)
  end
with oapp (ol : odd_list) (el : even_list) : odd_list :=
  match ol with
    | OCons x el' => OCons x (eapp el' el)
  end.

Theorem elength_eapp : forall el1 el2 : even_list,
                         elength (eapp el1 el2) = plus (elength el1) (elength el2).
Proof.
  induction el1; crush.
  (* We end up with having to prove following... *)
  (* S (olength (oapp o el2)) = S (olength o + elength el2) *)
Abort.
Check even_list_ind.

Scheme even_list_mut := Induction for even_list Sort Prop
                        with odd_list_mut := Induction for odd_list Sort Prop.

Check even_list_mut.

Theorem n_plus_0' : forall n : nat, plus n 0 = n.
Proof.
  apply nat_ind.
  Undo.
  apply (nat_ind (fun n => plus n 0 = n)); crush.
Qed.

(* Inductive term : Set := *)
(* | App : term -> term -> term    (* This is OK *) *)
(* | Abs : (term -> term) -> term. (* This is NOT OK because non strictly positive occurance of left side -> occurance of term *) *)

Print nat_rec.

Fixpoint plus_recursive (n : nat) : nat -> nat :=
  match n with
    | 0 => fun m => m
    | S n' => fun m => S (plus_recursive n' m)
  end.

Eval compute in plus_recursive 3 4.

(* We can define plus_recursive using nat_rec as follows
   nat_rec =  
   fun P : nat -> Set => nat_rect P 
      : forall P : nat -> Set, 
        P 0 -> (forall n : nat, P n -> P (S n)) -> forall n : nat, P n *)

Definition plus_rec : nat -> nat -> nat :=
  nat_rec (fun _ : nat => nat -> nat) (fun m => m) (fun _ r m => S (r m)).

Eval compute in plus_rec 3 4.

(* Proposition and the proof... *)
Definition done (T : Type) (x : T) := true.

(* Eval compute in done Prop True. *)

(* We can prove equivalence *)
Theorem plus_eqiv : plus_recursive = plus_rec.
  reflexivity.
Qed.

Print nat_rect.

Fixpoint nat_rect' (P : nat -> Type)
         (H0 : P 0)
         (HS : forall n, P n -> P (S n)) (n : nat) :=
  match n return P n with
    | 0 => H0
    | S n' => HS n' (nat_rect' P H0 HS n')
  end.

Fixpoint double (n : nat) : nat :=
  match n with
    | 0 => 0
    | S 0 => S (S 0)
    | S n' => S (S (double n'))
  end.

Eval compute in 3 + 4.

(* Reimplementing nat_ind *)
Section nat_ind'.
  (* Proposition we want to prove *)
  Variable P : nat -> Prop.

  (* proof of 0 case *)
  Hypothesis O_case : P 0.      

  (* proof of S case *)
  Hypothesis S_case : forall n : nat, P n -> P (S n).


  (* Turns out the following problem I had is solved by having Set Implicit Arguments directive at the top of the file.
  I think 
  The term "nat_ind' n'" has type "P n'" while it is expected to have type "nat".
  *)
  Fixpoint nat_ind' (n : nat) : P n :=
    match n with
      | 0 => O_case
      | S n' => S_case (nat_ind' n')
    end.

End nat_ind'.

Print nat.

Section even_list_mut'.
  Variable Peven : even_list -> Prop.
  Variable Podd : odd_list -> Prop.

  Hypothesis ENil_case : Peven ENil.
  Hypothesis ECons_case : forall (n : nat) (o : odd_list),
                            Podd o -> Peven (ECons n o).
  Hypothesis OCons_case : forall (n : nat) (e : even_list),
                            Peven e -> Podd (OCons n e).
  
  Fixpoint even_list_mut' (e : even_list) : Peven e :=
    match e with
      | ENil => ENil_case
      | ECons n o => ECons_case n (odd_list_mut' o)
    end
  with odd_list_mut' (o : odd_list) : Podd o :=
    match o with
      | OCons n e => OCons_case n (even_list_mut' e)
    end.

End even_list_mut'.

(* Neted Inductive Types *)

Inductive nat_tree : Set :=
  | NNode' : nat -> list nat_tree -> nat_tree.

Check nat_tree_ind.

Section All.
  Variable T : Type.
  Variable P : T -> Prop.

  Check nil.
  Check cons.

  Fixpoint All (ls : list T) : Prop :=
    match ls with
      | cons h t => P h /\ All t
      | Nil => True
    end.

End All.

Print True. (* Inductive True : Prop :=  I : True *)

Check I.

Locate I.
Locate True.
Locate "/\".

Check and.
Locate and.

Print and. 

Section map'.
  Variable T T' : Type.
  Variable F : T -> T'.

  Fixpoint map' (ls : list T) : list T' :=
    match ls with
      | nil => nil
      | cons h t => cons (F h) (map' t)
    end.

  Check sum.

  Fixpoint sum' (ls : list nat) : nat :=
    match ls with
      | nil => 0
      | cons h t => plus h (sum' t)
    end.

End map'.

Check NNode'.

(*
Fixpoint ntsize (tr : nat_tree) : nat :=
  match tr with
    | NNode' _ trs => S (sum (map' ntsize trs))
  end.
*)

Theorem true_neq_false : true <> false.
Proof.
  red.                          (* one step reduction *)
  intro H.

  Definition toProp (b : bool) := if b then True else False.

  change (toProp false).
  rewrite <- H.
  simpl.
  trivial.
Qed.

Theorem S_inj' : forall n m : nat, S n = S m -> n = m.
Proof.
  crush.
  Restart.
  intros n m H.
  change (pred (S n) = pred (S m)).
  rewrite H.
  reflexivity.
Qed.

Print S_inj'.