Add LoadPath "~/Dropbox/Projects/Coq/cpdtlib/".

Require Import List.
Import ListNotations.

Inductive binop : Set := Plus | Times.

Inductive exp : Set :=
| Const : nat -> exp
| Binop : binop -> exp -> exp -> exp.

Definition binopDenote (b : binop) : nat -> nat -> nat :=
  match b with
    | Plus => plus
    | Times => mult
  end.

Print binopDenote.

Fixpoint expDenote (e : exp) : nat :=
  match e with
    | Const n => n
    | Binop b e1 e2 => (binopDenote b) (expDenote e1) (expDenote e2)
  end.

Eval simpl in expDenote (Const 432).
Eval simpl in expDenote (Binop Plus (Const 2) (Const 5)).
Eval simpl in expDenote (Binop Times (Const 2) (Const 5)).

(* 2.1.2 Target Language *)

Inductive instr : Set :=
| iConst : nat -> instr
| iBinop : binop -> instr.

Definition prog := list instr.
Definition stack := list nat.

Definition instrDenote (i : instr) (s : stack) : option stack :=
  match i with
    | iConst n => Some (n :: s)
    | iBinop op =>
      match s with
        | arg1 :: arg2 :: s' =>
          Some ((binopDenote op) arg1 arg2 :: s')
        | _ => None
      end
  end.

Fixpoint progDenote (p : prog) (s : stack) : option stack :=
  match p with
    | [] => Some s
    | i :: p' =>
      match instrDenote i s with
        | None => None
        | Some s' => progDenote p' s'
      end
  end.

Definition mainProg (p : prog) : option stack :=
  progDenote p [].

Eval simpl in mainProg [ iConst 32 ].

(* 2.1.3 Translation *)

Fixpoint compile (e : exp) : prog :=
  match e with
    | Const n  => iConst n :: nil
    | Binop b e1 e2 => compile e2 ++ compile e1 ++ iBinop b :: nil
  end.

Eval simpl in compile (Const 32).
Eval simpl in compile (Binop Plus (Const 32) (Const 8)).
Eval simpl in progDenote (compile (Binop Plus (Const 32) (Const 8))).

(* 2.1.4 Translation Correctness *)

Theorem compile_correct :
  forall e, progDenote (compile e) nil = Some (expDenote e :: nil).
Proof.
Abort.

Lemma compile_correct' : forall e p s,
                           progDenote (compile e ++ p) s = progDenote p (expDenote e :: s).
Proof.
  intros e p s.
  induction e.
  (* e = Const n *)
  reflexivity.
  (* e = Binop e1 e2 *)
  simpl.
  rewrite app_assoc_reverse.
  rewrite app_assoc_reverse.
  rewrite app_assoc.
  rewrite app_assoc_reverse.
Abort.

Lemma compile_correct' : forall e p s,
                           progDenote (compile e ++ p) s = progDenote p (expDenote e :: s).
Proof.
  intros.