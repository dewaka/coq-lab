Add LoadPath "~/Dropbox/Projects/Coq/cpdtlib/".

Require Import List.

Set Implicit Arguments.

Check I.

Check (fun x : nat => x).
Check (fun x : True => x).
Check (fun x => x).

Check bool.
Check True.

Check tt.
Check unit.

Theorem unit_singleton : forall x : unit, x = tt.
(* Proof. induction x. reflexivity. Qed. *)
Proof. destruct x; reflexivity. Qed. (* akin to case analysis *)

Check unit_ind.
(* unit_ind *)
(*      : forall P : unit -> Prop, P tt -> forall u : unit, P u *)

Check Empty_set.

Theorem absurd : forall x : Empty_set, 2 + 2 = 5.
Proof. destruct 1. Qed. (* Unused x here is the first variable referred by a number *)

Check Empty_set_ind.
(* Empty_set_ind *)
(*      : forall (P : Empty_set -> Prop) (e : Empty_set), P e *)

Definition e2u (e : Empty_set) : unit := match e with end.

Print Empty_set.
(*
Inductive Empty_set : Set :=
There are no constructors to get an empty set ourselves.
*)

Check negb.

Theorem negb_inverse : forall b : bool, b = negb (negb b).
Proof.
  (* induction b; reflexivity. *)
  destruct b; reflexivity.
Qed.

Print Nat.add.

Theorem O_plus_n : forall n : nat, plus O n = n.
Proof. intro; reflexivity. Qed.
  
Theorem n_plus_O : forall n : nat, plus n O = n.
Proof.
  induction n. reflexivity. simpl. rewrite IHn. reflexivity.
  Restart.
  induction n; crush.
Qed.