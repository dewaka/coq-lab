Require Import List.
Require Import Arith.
Import ListNotations.

Section Sort.

  Variable A : Type.
  Variable le : A -> A -> Prop.
  Variable le_dec : forall (x y : A), {le x y} + {~le x y}.

  Inductive Sorted : list A -> Prop :=
  | Sorted_nil : Sorted nil
  | Sorted_cons : forall hd tl,
                    (forall x, In x tl -> le hd x) ->
                    Sorted tl ->
                    Sorted (hd :: tl).
     
  Check In.                     (* In : forall A : Type, A -> list A -> Prop *)

  (* Merging two sorted lists *)
  Fixpoint merge (l1 : list A) : list A -> list A :=
    match l1 with
      | nil => (fun l2 => l2)
      | h1 :: t1 => let fix merge_aux (l2 : list A) : list A :=
                        match l2 with
                          | nil => l1
                          | h2 :: t2 => if le_dec h1 h2
                                        then h1 :: merge t1 l2
                                        else h2 :: merge_aux t2
                        end
                    in merge_aux
    end.

End Sort.

Check merge.

Eval compute in lt (length [1;2;3]) 3.

Fixpoint less (m n : nat) : bool :=
  match m, n with
    | _, 0 => false
    | 0, _ => true
    | S m', S n' => less m' n'
  end.

Eval compute in less 3 4.
Eval compute in less 3 3.
Eval compute in leb 3 3.

Fixpoint tails {A : Type} (ls : list A) : list (list A) :=
  match ls with
    | nil => [nil]
    | _ :: t => ls :: tails t
  end.

Eval compute in tails [1; 2; 3].
Eval compute in tails [].

Section Filter.
  Variable A : Type.
  Variable p : A -> bool.

  Fixpoint filter (ls : list A) : list A :=
    match ls with
      | nil => nil
      | h :: t => if p h
                  then h :: filter t
                  else filter t
    end.
End Filter.

Check filter.

Section Fold.
  Variable A B : Type.
  Variable f : B -> A -> B.
  Variable g : A -> B -> B.

  Fixpoint foldl (b : B) (ls : list A) : B :=
    match ls with
      | nil => b
      | h :: t => foldl (f b h) t
    end.

  Fixpoint foldr (b : B) (ls : list A) : B :=
    match ls with
      | nil => b
      | h :: t => g h (foldr b t)
    end.

End Fold.

Implicit Arguments foldl [A B].
Implicit Arguments foldr [A B].

Check foldl.
Check foldr.

Extraction foldr.

Definition sum (ns : list nat) : nat := foldl plus 0 ns.
Definition sum' (ns : list nat) : nat := foldr plus 0 ns.

Eval compute in sum [1; 2; 3; 4; 5].
Eval compute in sum' [1; 2; 3; 4; 5].

Definition map' {A B : Type} (f : A -> B) (ls : list A) : list B :=
  let g x acc := f x :: acc
  in foldr g [] ls.

Eval compute in map S [1; 2; 3; 4].
Eval compute in map' S [1; 2; 3; 4].

(* This is chunking by one element *)
Fixpoint chunk1 {A : Type} (ls : list A) : list (list A) :=
  match ls with
    | nil => nil
    | h :: t => (h :: nil) :: chunk1 t
  end.

Eval compute in chunk1 [1; 2; 3; 4].

Fixpoint take_r {A : Type} (n : nat) (ls : list A) {struct ls} : (list A * list A) :=
  match n, ls with
    | 0, rs => (nil, rs)
    | _, nil => (nil, nil)
    | S n', h :: t => let (fs, rs) := take_r n' t
                      in (h :: fs, rs)
  end.

Eval compute in take_r 2 [1; 2; 3; 4].

Fixpoint drop {A : Type} (n : nat) (ls : list A) : list A :=
  match n, ls with
    | 0, _ => ls
    | S n', nil => nil
    | S n', _ :: ls' => drop n' ls'
  end.

Fixpoint take {A : Type} (n : nat) (ls : list A) {struct ls} : list A :=
  match n, ls with
    | 0, _ => nil
    | _, nil => nil
    | S n', h :: t => h :: take n' t
  end.

Check pred.
Eval compute in pred 3.
Eval compute in pred 0.
Eval compute in drop 3 [1; 2; 3; 4].
Eval compute in take 3 [1; 2; 3; 4].

(*
Fixpoint chunk {A : Type} (n : nat) (ls : list A) {struct ls} : list (list A) :=
  if less (length ls) n
  then [ls]
  else match ls with
         | nil => nil
         | _ :: t => let hs := take n ls
                     in hs :: chunk n (drop (pred n) t)
       end.
*)

(*
Fixpoint chunk {A : Type} (n : nat) (ls : list A) : list (list A) -> list (list A) :=
  if less (length ls) n
  then fun xs => xs
  else fun xs => xs.             (* Change! *)
*)                   